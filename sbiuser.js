
var locationLoadFlag = false;

var branchCodeFlag = false;

// To be called when form has to be submitted thru link



/* 

    Retail Banking-->Mypage-->AccountInformation-->quicklook(accountno) link 

    Form is submitted to quicklook.htm (QuickLookHandler)

*/
var quickLookForm = true;
function submitQuickLookForm(accountNo, branchCode) {
    //alert(accountNo+"<------>"+accountNo);
    if (quickLookForm) {
        quickLookForm = false;
        document.quickLookForm.accountNo.value = accountNo;
        document.quickLookForm.branchCode.value = branchCode;
        document.quickLookForm.submit();
    } else {
        alert("Previous 'Request' is inprogress");
    }

}


function checkRange(str1, str3) {

    var sdate = new Date(str1[2], str1[1] - 1, str1[0]);
    var edate = new Date(str3[2], str3[1] - 1, str3[0]);

    /** For calculating the number of days between 2 dates - Begin **/
    var num_milli = 1000 * 60 * 60 * 24;
    var ms1 = sdate.getTime();
    var ms2 = edate.getTime();
    var diff = Math.abs(ms1 - ms2);
    var noOfDaysInBetween = Math.floor(diff / num_milli);

    if (noOfDaysInBetween > 365) {

        return false;
    }

}




/* 

    Retail Banking-->Mypage-->Account Information--> view All Account(when there are more than 4 accounts) link 

    Form is submitted to viewallaccount.htm (ViewAllAccountHandler)

*/



function submitViewAllAccountForm(accountType, productDescription) {

    document.viewAllAccountForm.accountType.value = accountType;

    document.viewAllAccountForm.productDescription.value = productDescription;

    document.viewAllAccountForm.submit();

}

//<!-- SR - 105941 - Disable the link on click -->
function submitViewAllCorpAccountForm(accountType, productDescription, divType) {
    document.getElementById(divType).innerHTML = divType;
    document.viewAllAccountForm.accountType.value = accountType;
    document.viewAllAccountForm.productDescription.value = productDescription;
    document.viewAllAccountForm.submit();
}

// sr 87929 start

function submitViewAllAccountFormRetail(accountType, productDescription) {

    document.viewAllAccountForm.accountType.value = accountType;

    document.viewAllAccountForm.productDescription.value = productDescription;

    if (accountType == 'A5') {
        document.viewAllAccountForm.action = "viewalldepositaccount.htm";
    }

    document.viewAllAccountForm.submit();

}

// sr 87929 end

/* 

   Retail Banking-->Mypage-->Account Statement--> view statement button(sort by date) 

   Form is submitted to statementbydate.htm (AccountStatementHandler - multi action controller)

*/

function radioAccountStatement(accountNo) {

    document.getElementById("selectedAccount").innerHTML = accountNo;

}


//Added for CR 2975
function enquiryTxnDateValidation(frm, url) {

    var str = frm.startdate.value;
    var str1 = str.split('/');
    var startdate = str1[1] + " " + str1[0] + " " + str1[2];
    var str2 = frm.enddate.value;
    var str3 = str2.split('/')
    var enddate = str3[1] + " " + str3[0] + " " + str3[2];
    var str4 = frm.currentdate.value;
    var str5 = str4.split('/')
    var currentdate = str5[1] + " " + str5[0] + " " + str5[2];

    if (frm.startdate.value == 'dd-mmm-yyyy' || frm.startdate.value == 'dd-mm-yyyy' || frm.startdate.value == '')//changed for DEV-369
    {
        alert("Start Date should not be empty");
        return false;
    }
    if (frm.enddate.value == 'dd-mmm-yyyy' || frm.enddate.value == 'dd-mm-yyyy' || frm.enddate.value == '')//changed for DEV-369
    {
        alert("End Date should not be empty");
        return false;
    }
    if (new Date(startdate) > new Date(currentdate) || compareDates(str, str4) == false) {
        alert("Start date is greater than today(s) date");
        return false;
    }
    if (new Date(enddate) > new Date(currentdate) || compareDates(str2, str4) == false) {
        alert("End date is greater than today(s) date");
        return false;
    }
    if (new Date(startdate) > new Date(enddate) || compareDates(str, str2) == false) {
        alert("End date should be later than the start date");
        return false;
    }
    rdate = new Date(startdate);
    rangedate = rdate.setYear(rdate.getFullYear() + 1);



    if (new Date(enddate) >= rangedate || checkRange(str1, str3) == false) {
        alert("Date range for status enquiry cannot exceed one year");
        return false;
    }

    if (url == "enquirytxnstatus.htm" || url == "paymentenquirytxnstatus.htm") {
        document.enquiryTxnRangeDetails.action = "enquirytxnstatusdisplay.htm";
    }
    else if (url == "enquirytxnstatusmerchant.htm") {
        document.enquiryTxnRangeDetails.action = "enqtxnstatusdisplaymerchant.htm";
    }
    else if (url == "enquirytxnstatusvisa.htm") {
        document.enquiryTxnRangeDetails.action = "enquirytxnstatusdisplayvisa.htm";
    }
    else if (url == "topuphistory.htm") {

        document.enquiryTxnRangeDetails.action = "topuphistorydetails.htm";
    }
    else if (url == "temptopuphistory.htm") {

        document.enquiryTxnRangeDetails.action = "temptopuphistorydetails.htm";
    }
    document.enquiryTxnRangeDetails.submit();

    return true;
}

function enquiryTxnDateValidation_new(frm, url) {

    var str = frm.startdate.value;
    var str1 = str.split('/');
    var startdate = str1[1] + " " + str1[0] + " " + str1[2];
    var str2 = frm.enddate.value;
    var str3 = str2.split('/')
    var enddate = str3[1] + " " + str3[0] + " " + str3[2];
    var str4 = frm.currentdate.value;
    var str5 = str4.split('/')
    var currentdate = str5[1] + " " + str5[0] + " " + str5[2];

    if (frm.startdate.value == 'dd-mmm-yyyy' || frm.startdate.value == 'dd-mm-yyyy' || frm.startdate.value == '')//Changed for DEV-369
    {
        alert("Start Date should not be empty");
        return false;
    }
    if (frm.enddate.value == 'dd-mmm-yyyy' || frm.enddate.value == 'dd-mm-yyyy' || frm.enddate.value == '') //Changed for DEV-369
    {
        alert("End Date should not be empty");
        return false;
    }
    if (new Date(startdate) > new Date(currentdate) || compareDates(str, str4) == false) {
        alert("Start date is greater than today(s) date");
        return false;
    }
    if (new Date(enddate) > new Date(currentdate) || compareDates(str2, str4) == false) {
        alert("End date is greater than today(s) date");
        return false;
    }
    if (new Date(startdate) > new Date(enddate) || compareDates(str, str2) == false) {
        alert("End date should be later than the start date");
        return false;
    }
    rdate = new Date(startdate);
    rangedate = rdate.setYear(rdate.getFullYear() + 1);



    if (new Date(enddate) >= rangedate || checkRange(str1, str3) == false) {
        alert("Date range for status enquiry cannot exceed one year");
        return false;
    }

    if (url == "enquirytxnstatus.htm" || url == "paymentenquirytxnstatus.htm") {
        document.enquiryTxnRangeDetails.action = "enquirytxnstatusdisplay.htm";
    }
    else if (url == "enquirytxnstatusvisa.htm") {
        document.enquiryTxnRangeDetails.action = "enquirytxnstatusdisplayvisa.htm";
    }
    else if (url == "enquirytxnstatusmerchant.htm") {
        document.enquiryTxnRangeDetails.action = "enqtxnstatusdisplaymerchant.htm";
    }
    else if (url == "topuphistory.htm") {

        document.enquiryTxnRangeDetails.action = "topuphistorydetails.htm";
    }
    else if (url == "temptopuphistory.htm") {

        document.enquiryTxnRangeDetails.action = "temptopuphistorydetails.htm";
    }
    document.enquiryTxnRangeDetails.submit();

    return true;
}
function resetEnquiryRange(formObj) {
    document.getElementById("startDate").value = "dd-mmm-yyyy";
    document.getElementById("endDate").value = "dd-mmm-yyyy";

}

function resetEnquiryRange_new(formObj, url_flag) {
    if (url_flag == 'enquirytxnstatus.htm') {
        document.getElementById("datepicker1").value = "";
        document.getElementById("datepicker2").value = "";
    }
    else if (url_flag == 'enquirytxnstatusvisa.htm') {
        document.getElementById("datepicker3").value = "";
        document.getElementById("datepicker4").value = "";
    }

}
//End of CR 2975





function submitViewStatement() {

    var startdate = "";

    var enddate = "";

    var month = "";

    var year = "";
    /*mops*/
    var radiobuttonPeriod = "";

    if (document.viewStatementForm.accountNo.value == null || document.viewStatementForm.accountNo.value == '') {

        alert("Select an account");

        return false;

    }

    if (document.displayForm.radiobuttonPeriod[0].checked == true) {	// when by date is selected



        startdate = document.displayForm.startdate.value;

        enddate = document.displayForm.enddate.value;

        currentdate = document.displayForm.currentdate.value;

        radiobuttonPeriod = "date";

        if (startdate == "") {

            alert("Select start date");

            document.displayForm.startdate.focus();

            return false;

        }



        if (startdate == "Select Date") { // Added for CR 1255

            alert("Select start date");

            document.displayForm.startdate.focus();

            return false;

        }



        if (enddate == "") {

            alert("Select end date");

            document.displayForm.enddate.focus();

            return false;

        }
        // Added for CR 5218
        if (enddate == "Select Date") {

            alert("Select end date");

            document.displayForm.enddate.focus();

            return false;

        }
        //End of CR 5218


        /********** validation startdate currentdate and enddate(demand draft page) **************/



        if (compareDates(startdate, currentdate) == false) {

            alert("Start date is greater than today(s) date");

            return false;

        }

        if (compareDates(enddate, currentdate) == false) {

            alert("End date is greater than today(s) date");

            return false;

        }

        if (compareDates(startdate, enddate) == false) {

            alert("Start date is greater than end date");

            return false;

        }

        /********** validation start date currentdate and enddate **************/

        /********End of CR 5218-Validation for 6 months Starts here*************/

        if (!(six_months_validation())) {
            document.getElementById("startdate").focus();
            return false;
        }

        /**********End of CR 5218-Validation for 6 months ends here***************/

    } else { // when by month is selected 

        month = document.displayForm.comboMonth[document.displayForm.comboMonth.selectedIndex].value;

        year = document.displayForm.comboYear[document.displayForm.comboYear.selectedIndex].value;

        startdate = "01/" + month + "/" + year;

        var days = getNumerofDaysPerMonth(year, document.displayForm.comboMonth.selectedIndex);

        enddate = days + "/" + month + "/" + year;
        radiobuttonPeriod = "month";



    }

    document.viewStatementForm.fromDate.value = startdate;

    document.viewStatementForm.toDate.value = enddate;
    document.viewStatementForm.order.value = 1;
    document.viewStatementForm.noOfRows.value = document.displayForm.combonoOfRows.value;
    //mops -In closed account - radiobuttonperiod was missed.
    document.viewStatementForm.radiobuttonPeriod.value = radiobuttonPeriod;
    if (document.displayForm.view.checked) {

        document.viewStatementForm.target = "";

        document.viewStatementForm.action = "statementbydate.htm";

    } else if (document.displayForm.excelformat.checked) {

        // by uday start

        //var windowobj = window.open('/sbijava/blank.html','csv','width=780, height=500 ,status=1, scrollbars=1, location=0');
        var windowobj = "";

        document.viewStatementForm.fileFormat.value = "csvFormat";

        //	document.viewStatementForm.target = "csv";
        document.viewStatementForm.target = "";

        document.viewStatementForm.action = "downloadstatement.htm";

    } else if (document.displayForm.pdfformat.checked) {

        //var windowobj = window.open('/sbijava/blank.html','pdf','width=780, height=500 ,status=1, scrollbars=1, location=0');

        var windowobj = "";
        document.viewStatementForm.fileFormat.value = "pdfFormat";

        //	document.viewStatementForm.target = 'pdf';
        document.viewStatementForm.target = '';

        // by uday end	
        document.viewStatementForm.action = "downloadstatement.htm";

    }
    //Added For CR 5331 Starts
    else if (document.displayForm.asciiformat.checked) {

        var windowobj = window.open('blank.html', 'ascii', 'width=780, height=500 ,status=1, scrollbars=1, location=0');

        document.viewStatementForm.fileFormat.value = "asciiFormat";

        document.viewStatementForm.target = 'ascii';

        document.viewStatementForm.action = "downloadstatement.htm";

    }
    //Added For CR 5331 Ends

    document.viewStatementForm.submit();
}



function radioByDate(enddate) {

    var edate = new Date().getDate();	//sr 88666	
    var emonth = new Date().getMonth() + 1; //sr 88666

    document.displayForm.comboMonth.disabled = true;

    document.displayForm.comboYear.disabled = true;

    document.displayForm.startdate.disabled = false;

    document.displayForm.enddate.disabled = false;

    document.displayForm.enddate.value = enddate; //Added for CR 1255

    document.displayForm.startdate.value = "Select Date";

    //document.displayForm.enddate.value = "Select Date";//Added for CR 5218
    //sr 88666 start

    if (edate < 10) {
        edate = "0" + new Date().getDate()
    }
    if (emonth < 10) {
        emonth = "0" + emonth;
    }
    document.displayForm.enddate.value = edate + '/' + emonth + '/' + new Date().getFullYear();

    //sr 88666 end

    /*Added by lenin for Submit button enable starts*/
    var optButtonid = document.getElementById('Submit3');
    if (optButtonid.disabled == true) {
        optButtonid.disabled = false;
    }
    /*Added by lenin for Submit button enable ends*/

    document.images.startCalender.src = "/sbijava/images/calender_icon.gif";

    document.images.startCalender.useMap = "#Map";

    document.images.endCalender.src = "/sbijava/images/calender_icon.gif";

    document.images.endCalender.useMap = "#MapMap";

}

function radioByMonth() {

    /* alert("radioByMonth");*/
    document.displayForm.startdate.value = "";

    document.displayForm.enddate.value = "";

    document.displayForm.startdate.disabled = true;

    document.displayForm.enddate.disabled = true;

    document.displayForm.comboMonth.disabled = false;

    document.displayForm.comboYear.disabled = false;


    if (document.displayForm.radiobuttonPeriod[1].checked == false) {
        /*alert("radioByMonth inside last 6 months");*/
        document.displayForm.startdate.disabled = true;

        document.displayForm.enddate.disabled = true;

        document.displayForm.comboMonth.disabled = true;

        document.displayForm.comboYear.disabled = true;
    }

    var currentDate = new Date(); //Added for CR 1255

    var currentMonth = document.getElementById("date");

    currentMonth.selectedIndex = currentDate.getMonth();
    /*Added by lenin for Submit button enable starts*/
    var optButtonid = document.getElementById('Submit3');
    if (optButtonid.disabled == true) {
        optButtonid.disabled = false;
    }

    /*Added by lenin for Submit button enable ends*/

    document.images.startCalender.src = "/sbijava/images/calender_disabled_icon.gif";

    document.images.startCalender.useMap = "#Map1";

    document.images.endCalender.src = "/sbijava/images/calender_disabled_icon.gif";

    document.images.endCalender.useMap = "#Map1";



}

function submitPrintStatementBack() {

    document.printForm.submit();

}





/* Link details will not be shown*/



function submitPagination(pagenumber, count, pageNo) {

    document.paginationForm.pageNumber.value = pagenumber;

    document.paginationForm.max.value = pagenumber;

    document.paginationForm.count.value = count;

    document.paginationForm.pageNo.value = pageNo;

    document.paginationForm.submit();

}







function printAccountStatement() {

    window.print();

}



/* following function is called when onsubmit in the funds trfr page.Payment/transfer --> fundstransfer */

function validateFundsTransfer(type) {



    var debitAccountNo = document.fundsShowAccountForm.debitAccountNo.value;

    var creditAccountNo = document.fundsShowAccountForm.creditAccountNo.value;

    var debitBranchCode = document.fundsShowAccountForm.debitBranchCode.value;

    var creditBranchCode = document.fundsShowAccountForm.creditBranchCode.value;

    var amount = document.fundsTransferForm.TransferAmount.value;



    if (debitAccountNo == "") {

        alert("Select debit account");

        return false;

    }

    if (creditAccountNo == "") {

        alert("Select " + type + " account");

        return false;

    }


    // Modified for CR 5609 - eRD SI for 10 Years
    if (amount == "" && !isERDAccount()) {

        alert("Enter amount");

        document.fundsTransferForm.TransferAmount.focus();

        return false;

    }

    if (type == "PPF") {

        if (!validatemain("fundsTransferForm", "TransferAmount.checkmultiples.amount.amount")) {

            document.fundsTransferForm.TransferAmount.focus();

            return false;

        }

    }

    else {
        // Modified for CR 5609 - eRD SI for 10 Years
        if (!isERDAccount()) {
            if (!validatemain("fundsTransferForm", "TransferAmount.amtcheck.amount.amount")) {

                document.fundsTransferForm.TransferAmount.focus();

                return false;

            }
        }
        // Modified for CR 5609 - eRD SI for 10 Years
    }



    if ((debitAccountNo == creditAccountNo) && (debitBranchCode == creditBranchCode)) {

        alert("Source and destination accounts are the same. Select a different account");

        document.fundsTransferForm.TransferAmount.focus();

        return false;

    }



    if (document.fundsTransferForm.transactionRemarks.value != "") {

        if (!validatemain("fundsTransferForm", "transactionRemarks.alphanumerikcheck.remarks")) {



            document.fundsTransferForm.transactionRemarks.focus();

            return false;

        }

    }

    return true;

}



function submitEditableFundsTransfer() {

    if (!validatemain("fundsShowAccountForm", "amountTransfer.amtcheck.amount.amount")) {

        document.fundsShowAccountForm.amountTransfer.focus();

        return false;

    }



    document.fundsShowAccountForm.submit();

}



function submitFundsTransfer() {

    var debitAccountNo = document.fundsShowAccountForm.debitAccountNo.value;

    var creditAccountNo = document.fundsShowAccountForm.creditAccountNo.value;

    var debitBranchCode = document.fundsShowAccountForm.debitBranchCode.value;

    var creditBranchCode = document.fundsShowAccountForm.creditBranchCode.value;

    var amount = document.fundsTransferForm.TransferAmount.value;

    var debitProdDesc = "";

    var creditProdDesc = "";

    var debitBankSys = "";

    var creditBankSys = "";

    if (document.getElementById("dproductDisc") != null)

        debitProdDesc = document.getElementById("dproductDisc").value;

    if (document.getElementById("cproductDisc") != null)

        creditProdDesc = document.getElementById("cproductDisc").value;

    if (document.getElementById("dbankSystem") != null)

        debitBankSys = document.getElementById("dbankSystem").value;

    if (document.getElementById("cbankSystem") != null)

        creditBankSys = document.getElementById("cbankSystem").value;

    if (!validateFundsTransfer("credit"))

        return false;



    if (!isValidTransfer(debitAccountNo, creditAccountNo, 'Pri', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys)) {

        return false;

    }



    if (debitBranchCode != creditBranchCode) {

        if (amount > 100000) {

            alert("You cannot transfer an amount that exceeds 100000");

            document.fundsTransferForm.TransferAmount.focus();

            return false;

        }

    }



    document.fundsShowAccountForm.creditTxnCount.value = "1";

    document.fundsShowAccountForm.amountTransfer.value = document.fundsTransferForm.TransferAmount.value;

    document.fundsShowAccountForm.transactionRemarks.value = document.fundsTransferForm.transactionRemarks.value;

    document.fundsShowAccountForm.submit();

}



/* following function is called when onsubmit in the third party transfer page.Payment/transfer --> thirdPartyTransfer */



function submitThirdPartyTransfer(bankCode, txnView) {

    var debitAccountNo = document.fundsShowAccountForm.debitAccountNo.value;

    var creditAccountNo = document.fundsShowAccountForm.creditAccountNo.value;

    var debitBranchCode = document.fundsShowAccountForm.debitBranchCode.value;

    var creditBranchCode = document.fundsShowAccountForm.creditBranchCode.value;

    var amount = document.fundsTransferForm.TransferAmount.value;;

    var thirdPartyLimit = document.fundsShowAccountForm.thirdparty_limit.value;

    var accountLimit = document.fundsShowAccountForm.accountLimit.value;

    var debitProdDesc = "";

    var creditProdDesc = "";

    var debitBankSys = "";

    var creditBankSys = "";

    //	Added for CR 1205

    var debitProductCode = "";

    var creditProductCode = "";

    //	End of CR 1205

    if (document.getElementById("dproductDisc") != null)

        debitProdDesc = document.getElementById("dproductDisc").value;

    if (document.getElementById("cproductDisc") != null)

        creditProdDesc = document.getElementById("cproductDisc").value;

    if (document.getElementById("dbankSystem") != null)

        debitBankSys = document.getElementById("dbankSystem").value;

    if (document.getElementById("cbankSystem") != null)

        creditBankSys = document.getElementById("cbankSystem").value;

    //	Added for CR 1205	

    if (document.getElementById("dproductCode") != null)

        debitProductCode = document.getElementById("dproductCode").value;



    if (document.getElementById("cproductCode") != null)

        creditProductCode = document.getElementById("cproductCode").value;

    //	End of CR 1205

    if (!validateFundsTransfer('credit'))

        return false;



    if (bankCode == 0) {
        if (!isValidTransferForSBI(debitAccountNo, creditAccountNo, 'Pri', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode)) {

            return false;

        }

    }

    else if (!isValidTransfer(debitAccountNo, creditAccountNo, 'Pri', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode)) {

        return false;

    }


    if (parseFloat(amount) > parseFloat(accountLimit)) {

        alert("The transfer amount for " + creditAccountNo + " must be less than or equal to Rs." + accountLimit + "/-.");

        document.fundsTransferForm.TransferAmount.focus();

        return false;

    }

    //Commented for CR 5067

    //added for retail CR2633 starts

    var creditProductType = document.fundsShowAccountForm.creditAccountType.value;

    if (creditProductType == "B1") {
        //Added for CR 3081
        //commented by sridhar
        /*var PPFLimit = document.getElementById('PPFMiniLimit').value;	
         var PPFMaxLimit = document.getElementById('PPFMaxLimit').value;
            if(parseFloat(amount)<parseFloat(PPFLimit)){	
            alert("PPF deposit amount should be a minimum of Rs."+PPFLimit+".");
            return false;
            }	*/
        //Added for CR 3081 ends
        //commented by sridhar
        /*if(parseFloat(amount)>parseFloat(PPFMaxLimit))
        {
        alert("The Credit Limit for a PPF Account cannot exceed 777"+PPFMaxLimit+"/- ");
        return false;
        }*/
        if (amount % 5 != 0) {
            alert("Amount should be in multiples of 5");
            return false;
        }

    }

    //modified for SI schedule starts
    //added for PPF cr2633 starts
    if (creditProductType == "B1") {
        document.fundsShowAccountForm.action = "fundstransferdetails.htm";
    } else {
        document.fundsShowAccountForm.action = "lastfivetxndetails.htm";
    }
    // SI schedule ends
    //Added For Retail FT TP Scheduling

    if (document.fundsTransferForm.SchedulePay != null) {
        //modified for SI schedule starts
        if (document.fundsTransferForm.SchedulePay[1].checked || (document.fundsTransferForm.SchedulePay[2] != null && document.fundsTransferForm.SchedulePay[2].checked)) {
            var schDate = document.getElementById("Days").value + "/" + document.getElementById("Months").value + "/" + document.getElementById("Years").value;
            document.getElementById("schPayDate").value = schDate;
        }
        if (document.fundsTransferForm.SchedulePay[2] != null && document.fundsTransferForm.SchedulePay[2].checked) {
            var freqType = document.getElementById("freqType").value;
            var freqInDays = document.getElementById("freqInDays").value;
            var noOfPayments = document.getElementById("noOfPayments").value;
            var accType = eval(document.getElementById("creditAccountType")).value; /* Added for SI PPF account - martin */
            if (!purenumeric('fundsTransferForm', 'noOfPayments', 'Number Of Payments')) {
                return false;
            } else if (eval(noOfPayments) < 2) {
                alert("Number of payments should exceed 1");
                return false;
            }
            /* Added for SI PPF account - martin - Begin */
            if (accType == 'B1' && eval(noOfPayments) > 12) {
                alert("Number of payments should not exceed 12");
                return false;
            }

            if (isERDAccount() && eval(noOfPayments) > 119) {
                alert("Number of payments should not exceed ten years from the e-RD A/c creation date.");
                return false;
            }

            /* Added for SI PPF account - martin - End*/
            var yearInDays = 365 * 24 * 60 * 60 * 1000;
            var datesplit1 = (document.getElementById("schPayDate").value).split("/");
            date = datesplit1[0];
            month = datesplit1[1];
            var monArr = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
            for (var i = 0; i < 12; i++) {
                if (monArr[i] == month) {
                    month = i;
                    break;
                }
            }
            year = datesplit1[2];
            var startDate = new Date(year, month, date);
            var startTime = startDate.getTime();
            var dateoneYearAfter = new Date(startTime + yearInDays);
            var daysAdded = null;
            if (freqType == 'Days') {
                daysAdded = eval(freqInDays) * eval(noOfPayments);
            } else {
                daysAdded = eval(freqInDays) * 30 * eval(noOfPayments);
            }
            var daysAddedInTime = eval(daysAdded) * 24 * 60 * 60 * 1000;
            lastPayDate = new Date(daysAddedInTime + startTime);

            // Modified for CR : 5609 - Parameshwaran - Added conditon
            if (lastPayDate > dateoneYearAfter && !isERDAccount()) {
                alert("Period for standing instruction should not exceed one year");
                return false;
            }

            // Financial Year Calculation
            if (creditProductType == "B1") {
                var schDate = document.getElementById("Days").value + "/" + document.getElementById("Months").value + "/" + document.getElementById("Years").value;
                document.getElementById("schPayDate").value = schDate;

                if (!validatePPFFreq(parseFloat(amount))) {
                    return false;
                }
            }

            // Modified for CR : 5609 - Parameshwaran - Added condition

            document.fundsShowAccountForm.siFreqType.value = freqType;
            document.fundsShowAccountForm.siFreqInDays.value = freqInDays;
            document.fundsShowAccountForm.siNoOfPayments.value = noOfPayments;
            document.fundsShowAccountForm.action = "lastfivesitransactions.htm";
        }
        //modified for SI schedule ends
    }
    //Added for CR 5015		
    if (txnView == 'showLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'showLastFiveTransactions';
    }
    if (txnView == 'showSILastFiveTxn') {
        document.getElementById('lastFiveSITransactionFlag').value = 'showLastFiveSITransactions';
    }
    if (txnView == 'noShowLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'noShowLastFiveTransactions';
        document.getElementById('lastFiveSITransactionFlag').value = 'noShowLastFiveSITransactions';
    }
    if (creditProductType == "B1") {
        if (txnView == 'showLastFiveTxn')
            alert("If the credit account is a PPF account, the last five transaction details cannot be displayed here.");
    }
    //End of CR 5015
    document.fundsShowAccountForm.creditTxnCount.value = "1";

    document.fundsShowAccountForm.amountTransfer.value = document.fundsTransferForm.TransferAmount.value;

    document.fundsShowAccountForm.transactionRemarks.value = document.fundsTransferForm.transactionRemarks.value;

    document.fundsShowAccountForm.debitProductCode.value = debitProductCode;	//	Added for CR 1205

    document.fundsShowAccountForm.creditProductCode.value = creditProductCode;	//	Added for CR 1205


    document.fundsShowAccountForm.submit();

}



function saveNickName(nickName) {

    document.fundsShowAccountForm.nickName.value = nickName;

}



function submitLoanPartPayment() {

    var debitBranchCode = document.fundsShowAccountForm.debitBranchCode.value;

    var creditBranchCode = document.fundsShowAccountForm.creditBranchCode.value;

    var debitAccountNo = document.fundsShowAccountForm.debitAccountNo.value;

    var creditAccountNo = document.fundsShowAccountForm.creditAccountNo.value;

    var debitProdDesc = "";

    var creditProdDesc = "";

    var debitBankSys = "";

    var creditBankSys = "";

    //	Added for CR 1256

    var debitProductCode = "";

    var creditProductCode = "";

    //	End of CR 1256

    if (document.getElementById("dproductDisc") != null)

        debitProdDesc = document.getElementById("dproductDisc").value;

    if (document.getElementById("cproductDisc") != null)

        creditProdDesc = document.getElementById("cproductDisc").value;

    if (document.getElementById("dbankSystem") != null)

        debitBankSys = document.getElementById("dbankSystem").value;

    if (document.getElementById("cbankSystem") != null)

        creditBankSys = document.getElementById("cbankSystem").value;

    //	Added for CR 1256	

    if (document.getElementById("dproductCode") != null)

        debitProductCode = document.getElementById("dproductCode").value;

    if (document.getElementById("cproductCode") != null)

        creditProductCode = document.getElementById("cproductCode").value;

    //	End of CR 1256	

    if (!validateFundsTransfer('loan'))

        return false;



    if (debitBranchCode != creditBranchCode) {

        alert("Due to some technical problems, we are unable to provide inter-branch transfers now. We regret the inconvenience.");

        return false;

    }

    if (!isValidTransfer(debitAccountNo, creditAccountNo, 'Pri', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode)) {

        return false;

    }

    document.fundsShowAccountForm.creditTxnCount.value = "1";

    document.fundsShowAccountForm.amountTransfer.value = document.fundsTransferForm.TransferAmount.value;

    document.fundsShowAccountForm.transactionRemarks.value = document.fundsTransferForm.transactionRemarks.value;

    document.fundsShowAccountForm.submit();

}



function submitCreditToPPF() {

    var debitBranchCode = document.fundsShowAccountForm.debitBranchCode.value;

    var creditBranchCode = document.fundsShowAccountForm.creditBranchCode.value;

    var debitAccountNo = document.fundsShowAccountForm.debitAccountNo.value;

    var creditAccountNo = document.fundsShowAccountForm.creditAccountNo.value;

    var sbiPPFTransLimit = document.fundsShowAccountForm.sbiPPFTransLimit.value;

    var amount = document.fundsTransferForm.TransferAmount.value;;

    var debitProdDesc = "";

    var creditProdDesc = "";

    var debitBankSys = "";

    var creditBankSys = "";

    //	Added for CR 1256

    var debitProductCode = "";

    var creditProductCode = "";

    //	End of CR 1256

    //Added For Retail Scheduling for CR2633 on 25.07.08 starts
    if (document.fundsTransferForm.SchedulePay != null) {
        if (document.fundsTransferForm.SchedulePay[1].checked) {
            var schDate = document.getElementById("Days").value + "/" + document.getElementById("Months").value + "/" + document.getElementById("Years").value;
            document.getElementById("schPayDate").value = schDate;
        }
    }
    //Added For Retail Scheduling for CR2633 on 25.07.08 ends

    if (document.getElementById("dproductDisc") != null)

        debitProdDesc = document.getElementById("dproductDisc").value;

    if (document.getElementById("cproductDisc") != null)

        creditProdDesc = document.getElementById("cproductDisc").value;

    if (document.getElementById("dbankSystem") != null)

        debitBankSys = document.getElementById("dbankSystem").value;

    if (document.getElementById("cbankSystem") != null)

        creditBankSys = document.getElementById("cbankSystem").value;

    //	Added for CR 1256	

    if (document.getElementById("dproductCode") != null)

        debitProductCode = document.getElementById("dproductCode").value;

    if (document.getElementById("cproductCode") != null)

        creditProductCode = document.getElementById("cproductCode").value;

    //	End of CR 1256

    if (!validateFundsTransfer('PPF'))

        return false;



    if (!isValidTransfer(debitAccountNo, creditAccountNo, 'Pri', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode)) {

        return false;

    }


    if (parseFloat(amount) > parseFloat(sbiPPFTransLimit)) {

        alert("The transfer amount for " + creditAccountNo + " must be less than SBI limit of Rs." + sbiPPFTransLimit + "/-.");

        document.fundsTransferForm.TransferAmount.focus();

        return false;

    }



    //added for cr 2633 PPF starts


    if (debitBranchCode != creditBranchCode) {
        alert("Facility to transfer funds to a PPF account held in other branches/banks is currently not supported by the system. We regret the inconvenience.");
        return false;

    }


    var radioPPF = document.fundsTransferForm.radiobuttonPPFMode;

    if (radioPPF != null) {

        for (count = 0; count < radioPPF.length; count++) {

            if (radioPPF[count].checked == true)

                document.fundsShowAccountForm.PPFMode.value = radioPPF[count].value;

        }

    }

    document.fundsShowAccountForm.creditTxnCount.value = "1";

    document.fundsShowAccountForm.amountTransfer.value = document.fundsTransferForm.TransferAmount.value;

    document.fundsShowAccountForm.transactionRemarks.value = document.fundsTransferForm.transactionRemarks.value;

    document.fundsShowAccountForm.submit();

}



function fundsTransferValue(accountNo, branchCode, accountType, type, limit, ifscCode) {





    if (type == "credit") {

        document.getElementById('showSelc').style.display = '';

        document.fundsShowAccountForm.creditAccountNo.value = accountNo;

        document.getElementById("selc").innerHTML = accountNo;

        document.fundsShowAccountForm.creditBranchCode.value = branchCode;

        document.fundsShowAccountForm.creditAccountType.value = accountType;

        if (limit != null) {

            document.fundsShowAccountForm.accountLimit.value = limit;

        }

        if (ifscCode != null)

            document.fundsShowAccountForm.ifscCode.value = ifscCode;

    }

    if (type == "debit") {

        document.fundsShowAccountForm.debitAccountNo.value = accountNo;

        document.getElementById("selectedAccount").innerHTML = accountNo;

        document.fundsShowAccountForm.debitBranchCode.value = branchCode;

        document.fundsShowAccountForm.debitAccountType.value = accountType;

    }

}



function resetFundsTransfer(element1, element2) {

    if (document.getElementById(element1) != null)

        document.getElementById(element1).innerHTML = "";

    if (document.getElementById(element2) != null)

        document.getElementById(element2).innerHTML = "";

    if (document.getElementById("showSeld") != null)

        document.getElementById("showSeld").style.display = "none"

    if (document.getElementById("showSelc") != null)

        document.getElementById("showSelc").style.display = "none"

    if (document.fundsShowAccountForm.debitAccountNo != null)

        document.fundsShowAccountForm.debitAccountNo.value = "";

    if (document.fundsShowAccountForm.debitBranchCode != null)

        document.fundsShowAccountForm.debitBranchCode.value = "";

    if (document.fundsShowAccountForm.debitAccountType != null)

        document.fundsShowAccountForm.debitAccountType.value = "";

    if (document.fundsShowAccountForm.accountLimit != null)

        document.fundsShowAccountForm.accountLimit.value = "";

    if (document.fundsShowAccountForm.creditBranchCode != null)

        document.fundsShowAccountForm.creditBranchCode.value = "";

    if (document.fundsShowAccountForm.creditAccountType != null)

        document.fundsShowAccountForm.creditAccountType.value = "";

    if (document.fundsShowAccountForm.transactionRemarks != null)

        document.fundsShowAccountForm.transactionRemarks.value = "";

    if (document.fundsShowAccountForm.amountTransfer != null)

        document.fundsShowAccountForm.amountTransfer.value = "";

    if (document.fundsShowAccountForm.creditTxnCount != null)

        document.fundsShowAccountForm.creditTxnCount.value = "";

    if (document.fundsShowAccountForm.creditAccountNo != null)

        document.fundsShowAccountForm.creditAccountNo.value = "";

    if (document.fundsShowAccountForm.PPFMode != null)

        document.fundsShowAccountForm.PPFMode.value = "";
    //added for SI schedule starts
    //Added for CR 2992
    if (document.getElementById("instruction") != null) {
        document.getElementById("instruction").style.display = "none";
        document.getElementById("schedule").style.display = "none";
        // Added for CR 5609
        displayNonErdSiFields();
        // Added for CR 5609	
    }
    //added for SI schedule ends
    //For CR 5015
    if (document.getElementById("viewLastFiveDetails") != null)
        document.getElementById("viewLastFiveDetails").style.display = "";
    if (document.getElementById("viewLastSIDetails") != null) {
        document.getElementById("viewLastSIDetails").style.display = "none";
    }
    //CR 5015 - Ends
}





function demandDraftAddress(type) {



    if (type == "Self") {

        document.demandDraftForm.radioCourier.checked = false;
        document.demandDraftValueForm.ddAddressType.value = "";

        document.getElementById('deliveryModeDiv').style.display = "none";

        document.demandDraftValueForm.ddDeliveryMode.value = "Self";

    }



    if (type == "Courier") {



        document.demandDraftForm.radioSelf.checked = false;
        document.demandDraftValueForm.ddDeliveryMode.value = "Courier";
        document.demandDraftValueForm.ddAddressType.value = "Registered";


    }

    if (type == "New") {

        document.demandDraftForm.radioCourier.checked = true;
        document.demandDraftForm.radioSelf.checked = false;
        document.demandDraftValueForm.ddAddressType.value = "New";

    }

}

function demandDraftValue(accountNo, branchCode, accountType) {

    document.demandDraftValueForm.debitAccountNo.value = accountNo;

    document.getElementById("selectedAccount").innerHTML = accountNo;

    document.demandDraftValueForm.debitBranchCode.value = branchCode;

    document.demandDraftValueForm.debitAccountType.value = accountType;



}

function validateBranchCode() {

    branchDetails.validateBranchCode(findBranchCode, document.getElementById('branchCode').value);

}

function validateBranchCodeCSS() {
    //DWR2 IMPL
    branchDetails.validateBranchCode(document.getElementById('branchCode').value, findBranchCode);

}


function validateBranchCodeByDWR() {

    branchDetails.validateBranchCode(document.getElementById('branchCode').value, { callback: function (data) { findBranchCode(data); } });

}



// CR 5321
function submitDemandDraft(bankCode, mobileNo, txnView) {

    var flag = false;

    var branchCodeList = document.getElementById('branchCodeArray').value;

    var debitBranchCode = document.getElementById('debitBranchCode').value;

    var creditBranchCode = document.getElementById('creditBranchCode').value;

    var userddLimit = document.demandDraftValueForm.ddLimit.value;

    var sbiddLimit = document.demandDraftValueForm.sbiddLimit.value;

    var amount = document.demandDraftForm.draftAmount.value;

    if (document.demandDraftValueForm.debitAccountNo.value == "") {

        alert("Select account");

        return false;
    }

    if (document.demandDraftForm.draftAmount.value == "") {

        alert("Enter draft amount");

        document.demandDraftForm.draftAmount.focus();

        return false;

    }

    if (!validatemain("demandDraftForm", "draftAmount.amtcheck.Draft amount.amount"))

        return false;



    if (document.demandDraftForm.draftTo.value == "") {

        alert("Enter the name in favour of whom the Demand Draft is drawn");

        document.demandDraftForm.draftTo.focus();

        return false;

    }



    // cr -1609 start

    var favour = document.demandDraftForm.draftTo.value;

    if ((document.getElementById("dbankSystem").value).toLowerCase() == "noncore") {

        if (favour.length > 25) {

            alert("In favour of should not be more than 25 characters");

            document.demandDraftForm.draftTo.value = "";

            document.demandDraftForm.draftTo.focus();

            return false;

        }

    }

    // cr -1609 end 

    if (!validatemain("demandDraftForm", "draftTo.alphanumerikcheck.Favouring")) {

        document.demandDraftForm.draftTo.focus();

        return false;
    }

    var favouring = document.demandDraftForm.draftTo.value;

    favouring = favouring.toLowerCase();

    if (favouring == "self") {
        alert("Demand draft favouring 'Self' is not permitted.");

        document.demandDraftForm.draftTo.focus();

        return false;

    }

    if (parseFloat(amount) > parseFloat(userddLimit)) {

        alert("Draft amount should be less than user's demand draft limit, Rs." + userddLimit + "/-");

        document.demandDraftForm.draftAmount.focus();

        return false;
    }

    if (parseFloat(amount) > parseFloat(sbiddLimit)) {

        alert("Draft amount should be less than SBI demand draft limit, Rs." + sbiddLimit + "/-");

        document.demandDraftForm.draftAmount.focus();

        return false;
    }


    if (document.demandDraftForm.remarks.value != "") {

        if (!validatemain("demandDraftForm", "remarks.alphanumerikcheck.Purpose")) {

            document.demandDraftForm.remarks.focus();

            return false;
        }
    }

    /*Change for New Demand Draft - Branch Code on 20th Sep 2013 start */
    if (bankCode == '0') {
        if (document.getElementById('branchCode') != null) {

            document.demandDraftValueForm.ddCreditBranch.value = document.getElementById('branchName').value;

            /*	if(document.getElementById('locationName').options[document.getElementById('locationName').selectedIndex].text == "--Select Location--")
        
                {
        
                    alert("Select location");
        
                    return false;
        
                }
        
                if(document.getElementById('branchName').value == "--Select Branch--")
        
                {
        
                    alert("Select branch");
        
                    return false;
        
                }*/
            var debitBranchCode = document.getElementById("debitBranchCode").value;

            var creditBranchCode = document.getElementById("creditBranchCode").value;
            if (bankCode != 0) {
                if (debitBranchCode == creditBranchCode) {
                    alert('You cannot issue a DD payable at your debit-account branch. Please select a different branch to proceed.');
                    return false;
                }
            }

        }
    }

    if (bankCode != '0') {
        if (document.getElementById('branchCode') != null) {

            document.demandDraftValueForm.ddCreditBranch.value = document.getElementById('branchName').value;

            if (document.getElementById('locationName') != null) {

                if (document.getElementById('locationName').options[document.getElementById('locationName').selectedIndex].text == "--Select Location--") {

                    alert("Select location");

                    return false;

                }


                if (document.getElementById('branchName').value == "--Select Branch--") {

                    alert("Select branch");

                    return false;

                }
            }
            var debitBranchCode = document.getElementById("debitBranchCode").value;

            var creditBranchCode = document.getElementById("creditBranchCode").value;

            if (debitBranchCode == creditBranchCode) {
                alert('You cannot issue a DD payable at your debit-account branch. Please select a different branch to proceed.');
                return false;
            }


        }

    }

    //document.demandDraftValueForm.ddCreditBranch.value = document.demandDraftForm.branchCode.value;

    //CR-2880	start
    if ((document.demandDraftForm.branchCode.value).length < 5) {
        alert("Please enter 5 digit branch code.");
        document.demandDraftForm.branchCode.focus();
        return false;
    }
    //CR-2880	end

    if (!validatemain('demandDraftForm', 'branchCode.amtcheck.Branch code.number')) {

        document.getElementById('branchCode').focus();

        return false;

    }
    //CR-2880 start
    var bcode = document.getElementById('branchCode').value;

    document.demandDraftValueForm.ddCreditBranch.value = bcode;
    if (bankCode == '0' && bcode.substring(0, 1) == '1') {
        aBranchCode = "A" + (bcode).substring(bcode, 1, 4);
        document.demandDraftValueForm.ddCreditBranch.value = aBranchCode;

    }

    /*if(bankCode=='0' && bcode.substring(0,1)=='0' ){	
        cBranchCode = "0"+(bcode).substring(bcode,1,4);
        document.demandDraftValueForm.ddCreditBranch.value=cBranchCode;		

    }
	
    if(bankCode=='0' && bcode.substring(0,1)=='6' ){	
        dBranchCode = "6"+(bcode).substring(bcode,1,4);
        document.demandDraftValueForm.ddCreditBranch.value=dBranchCode;		
    }
	
    if(bankCode=='0' && bcode.substring(0,1)=='3' ){	
        bBranchCode = "3"+(bcode).substring(bcode,1,4);
        document.demandDraftValueForm.ddCreditBranch.value=bBranchCode;		
    }
	
    if(bankCode=='2' && bcode.substring(0,1)=='2' ){	
        eBranchCode = "2"+(bcode).substring(bcode,1,4);
        document.demandDraftValueForm.ddCreditBranch.value=eBranchCode;		
    	
    }*/


    //CR-2880 end
    if (bankCode != 0) {
        if (document.demandDraftValueForm.ddCreditBranch.value == document.getElementById('debitBranchCode').value) {
            alert('You cannot issue a DD payable at your debit-account branch. Please select a different branch to proceed.');
            document.getElementById('branchCode').focus();
            return false;
        }
    }

    var branchCode = document.getElementById('branchCode').value;

    if (branchCodeList.indexOf("|" + branchCode + "|") == -1) {

        alert("Invalid branch code");

        document.getElementById('branchCode').focus();

        return false;

    }



    /*	else{
    
            alert("Select the branch code option if you know the branch code. Else, select location.");		
    
            return false;
    
        }*/

    /*Change for New Demand Draft - Branch Code on 20th Sep 2013 end */

    if (document.demandDraftForm.radioSelf.checked == false && document.demandDraftForm.radioCourier.checked == false) {

        alert("Select delivery mode");

        return false;

    }

    if (document.demandDraftForm.locationName != null)

        document.demandDraftValueForm.ddCreditLocation.value = document.demandDraftForm.locationName[document.demandDraftForm.locationName.selectedIndex].value;

    if (document.demandDraftForm.radioSelf.checked == true) {

        document.demandDraftValueForm.ddStreetAddress1.value = "DD will be picked up by the   ";

        document.demandDraftValueForm.ddStreetAddress2.value = "user personally from branch";

        document.demandDraftValueForm.ddState.value = "";

        document.demandDraftValueForm.ddPin.value = "";

        document.demandDraftValueForm.ddCity.value = "";

        document.demandDraftValueForm.ddCountry.value = "";



    }
    // Added for Dev 324 
    if (document.demandDraftForm.radioCourier.checked == true) {
        document.demandDraftValueForm.ddStreetAddress1.value = document.getElementById("regaddress1").innerHTML;

        document.demandDraftValueForm.ddStreetAddress2.value = document.getElementById("regaddress2").innerHTML;

        document.demandDraftValueForm.ddState.value = document.getElementById("regstate").innerHTML;

        document.demandDraftValueForm.ddPin.value = document.getElementById("regpin").innerHTML;

        document.demandDraftValueForm.ddCity.value = document.getElementById("regcity").innerHTML;

        document.demandDraftValueForm.ddCountry.value = document.getElementById("regcountry").innerHTML;

    }


    document.demandDraftValueForm.amountTransfer.value = document.demandDraftForm.draftAmount.value;

    document.demandDraftValueForm.branchCodeName.value = document.demandDraftForm.branchCode.value;


    document.demandDraftValueForm.ddTo.value = document.demandDraftForm.draftTo.value;

    //document.demandDraftValueForm.ddRemarks.value = document.demandDraftForm.ddRemarks.value;
    ddRemarks();
    document.demandDraftValueForm.creditTxnCount.value = "1";

    document.demandDraftValueForm.transactionName.value = "DD";


    //Added for CR 5015	
    if (txnView == 'showLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'showLastFiveTransactions';
    }
    if (txnView == 'noShowLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'noShowLastFiveTransactions';
    }
    //End of CR 5015
    //CR 5578 
    if (bankCode == 0) {
        if (document.demandDraftValueForm.ddCreditBranch.value == document.getElementById('debitBranchCode').value) {
            if (!confirm("A Demand Draft cannot be issued payable at the debit branch.\nInstead a Banker\'s cheque will be issued.Do you want to Proceed?")) {
                return false;
            }
        }
    }
    fnAlertMobileDetails(bankCode, mobileNo); // CR 5321

    document.demandDraftValueForm.action = "lastfivetxndetails.htm";	//CR 897&972 

    document.demandDraftValueForm.submit();



}


//Added for Associate Banks flow DD

function submitDemandDraftABs(bankCode, mobileNo, txnView) {
    var flag = false;

    var branchCodeList = document.getElementById('branchCodeArray').value;

    var debitBranchCode = document.getElementById('debitBranchCode').value;

    var userddLimit = document.demandDraftValueForm.ddLimit.value;

    var sbiddLimit = document.demandDraftValueForm.sbiddLimit.value;

    var amount = document.demandDraftForm.draftAmount.value;



    if (document.demandDraftValueForm.debitAccountNo.value == "") {

        alert("Select account");

        return false;

    }

    if (document.demandDraftForm.draftAmount.value == "") {

        alert("Enter draft amount");

        document.demandDraftForm.draftAmount.focus();

        return false;

    }

    if (!validatemain("demandDraftForm", "draftAmount.amtcheck.Draft amount.amount"))

        return false;



    if (document.demandDraftForm.draftTo.value == "") {

        alert("Enter the name in favour of whom the Demand Draft is drawn");

        document.demandDraftForm.draftTo.focus();

        return false;

    }



    // cr -1609 start

    var favour = document.demandDraftForm.draftTo.value;

    if ((document.getElementById("dbankSystem").value).toLowerCase() == "noncore") {

        if (favour.length > 25) {

            alert("In favour of should not be more than 25 characters");

            document.demandDraftForm.draftTo.value = "";

            document.demandDraftForm.draftTo.focus();

            return false;

        }





    }

    // cr -1609 end 

    if (!validatemain("demandDraftForm", "draftTo.alphanumerikcheck.Favouring")) {

        document.demandDraftForm.draftTo.focus();

        return false;

    }

    var favouring = document.demandDraftForm.draftTo.value;

    favouring = favouring.toLowerCase();

    if (favouring == "self") {
        alert("Demand draft favouring 'Self' is not permitted.");

        document.demandDraftForm.draftTo.focus();

        return false;

    }

    if (parseFloat(amount) > parseFloat(userddLimit)) {

        alert("Draft amount should be less than user's demand draft limit, Rs." + userddLimit + "/-");

        document.demandDraftForm.draftAmount.focus();

        return false;



    }

    if (parseFloat(amount) > parseFloat(sbiddLimit)) {

        alert("Draft amount should be less than SBI demand draft limit, Rs." + sbiddLimit + "/-");

        document.demandDraftForm.draftAmount.focus();

        return false;



    }



    if (document.demandDraftForm.remarks.value != "") {

        if (!validatemain("demandDraftForm", "remarks.alphanumerikcheck.Purpose")) {

            document.demandDraftForm.remarks.focus();

            return false;

        }

    }

    if (document.getElementById('radioBranchSelect').checked) {

        document.demandDraftValueForm.ddCreditBranch.value = document.getElementById('branchName').value;

        if (document.getElementById('locationName').options[document.getElementById('locationName').selectedIndex].text == "--Select Location--") {

            alert("Select location");

            return false;

        }

        if (document.getElementById('branchName').value == "--Select Branch--") {

            alert("Select branch");

            return false;

        }
        var debitBranchCode = document.getElementById("debitBranchCode").value;
        var CreditBranchCode = document.getElementById('branchName').value;
        if (bankCode != 0) {
            if (debitBranchCode == CreditBranchCode) {
                alert('You cannot issue a DD payable at your debit-account branch. Please select a different branch to proceed.');
                return false;
            }
        }

    } else if (document.getElementById('radioBranchText').checked) {

        document.demandDraftValueForm.ddCreditBranch.value = document.demandDraftForm.branchCode.value;

        //CR-2880	start
        if ((document.demandDraftForm.branchCode.value).length < 5) {
            alert("Please enter 5 digit branch code.");
            document.demandDraftForm.branchCode.focus();
            return false;
        }
        //CR-2880	end

        if (!validatemain('demandDraftForm', 'branchCode.amtcheck.Branch code.number')) {

            document.getElementById('branchCode').focus();

            return false;

        }
        //CR-2880 start
        var bcode = document.getElementById('branchCode').value;

        if (bankCode == '0' && bcode.substring(0, 1) == '1') {
            aBranchCode = "A" + (bcode).substring(bcode, 1, 4);
            document.demandDraftValueForm.ddCreditBranch.value = aBranchCode;

        }

        //CR-2880 end
        if (bankCode != 0) {
            if (document.demandDraftValueForm.ddCreditBranch.value == document.getElementById('debitBranchCode').value) {
                alert('You cannot issue a DD payable at your debit-account branch. Please select a different branch to proceed.');
                document.getElementById('branchCode').focus();
                return false;
            }
        }

        var branchCode = document.getElementById('branchCode').value;

        if (branchCodeList.indexOf("|" + branchCode + "|") == -1) {

            alert("Invalid branch code");

            document.getElementById('branchCode').focus();

            return false;

        }



    } else {

        alert("Select the branch code option if you know the branch code. Else, select location.");

        return false;

    }

    if (document.demandDraftForm.radioSelf.checked == false && document.demandDraftForm.radioCourier.checked == false) {

        alert("Select delivery mode");

        return false;

    }

    if (document.demandDraftForm.locationName != null)

        document.demandDraftValueForm.ddCreditLocation.value = document.demandDraftForm.locationName[document.demandDraftForm.locationName.selectedIndex].value;

    if (document.demandDraftForm.radioSelf.checked == true) {

        document.demandDraftValueForm.ddStreetAddress1.value = "DD will be picked up by the   ";

        document.demandDraftValueForm.ddStreetAddress2.value = "user personally from branch";

        document.demandDraftValueForm.ddState.value = "";

        document.demandDraftValueForm.ddPin.value = "";

        document.demandDraftValueForm.ddCity.value = "";

        document.demandDraftValueForm.ddCountry.value = "";



    }
    // Added for Dev 324 
    if (document.demandDraftForm.radioCourier.checked == true) {
        document.demandDraftValueForm.ddStreetAddress1.value = document.getElementById("regaddress1").innerHTML;

        document.demandDraftValueForm.ddStreetAddress2.value = document.getElementById("regaddress2").innerHTML;

        document.demandDraftValueForm.ddState.value = document.getElementById("regstate").innerHTML;

        document.demandDraftValueForm.ddPin.value = document.getElementById("regpin").innerHTML;

        document.demandDraftValueForm.ddCity.value = document.getElementById("regcity").innerHTML;

        document.demandDraftValueForm.ddCountry.value = document.getElementById("regcountry").innerHTML;

    }


    document.demandDraftValueForm.amountTransfer.value = document.demandDraftForm.draftAmount.value;

    document.demandDraftValueForm.ddTo.value = document.demandDraftForm.draftTo.value;

    //document.demandDraftValueForm.ddRemarks.value = document.demandDraftForm.ddRemarks.value;
    ddRemarks();
    document.demandDraftValueForm.creditTxnCount.value = "1";

    document.demandDraftValueForm.transactionName.value = "DD";


    //Added for CR 5015	
    if (txnView == 'showLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'showLastFiveTransactions';
    }
    if (txnView == 'noShowLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'noShowLastFiveTransactions';
    }
    //End of CR 5015
    //CR 5578 
    if (bankCode == 0) {
        if (document.demandDraftValueForm.ddCreditBranch.value == document.getElementById('debitBranchCode').value) {
            if (!confirm("A Demand Draft cannot be issued payable at the debit branch.\nInstead a Banker\'s cheque will be issued.Do you want to Proceed?")) {
                return false;
            }
        }
    }
    fnAlertMobileDetails(bankCode, mobileNo); // CR 5321

    document.demandDraftValueForm.action = "lastfivetxndetails.htm";	//CR 897&972 

    document.demandDraftValueForm.submit();



}

function findBranchCode(data) {

    branchCodeFlag = data;

}

function branchOrLocation(type) {

    if (type == 'branchCodeSelect') {

        document.getElementById('branchCodeText').style.display = 'none';

        document.getElementById('branchCodeSelect').style.display = '';

    } else {

        document.getElementById('branchCodeText').style.display = '';

        document.getElementById('branchCodeSelect').style.display = 'none';
    }

}

function resetDemandDraft() {

    document.getElementById('registeredAddress').style.display = "none";

    document.getElementById("seld").innerHTML = " ";

    document.getElementById("showSeld").style.display = "none";

    document.demandDraftValueForm.amountTransfer.value = "";

    document.demandDraftValueForm.ddTo.value = "";

    document.demandDraftValueForm.ddRemarks.value = "";

    document.demandDraftValueForm.creditTxnCount.value = "";

    document.demandDraftValueForm.transactionName.value = "";

    document.demandDraftValueForm.ddCreditState.value = "";

    document.demandDraftValueForm.ddCreditBranch.value = "";

    document.demandDraftValueForm.ddCreditLocation.value = "";

    document.demandDraftValueForm.debitAccountNo.value = "";

    document.demandDraftForm.draftAmount.value = "";

    document.demandDraftForm.draftTo.value = "";

}



function submitLoanClosurePayment(bankCode, mobileNo) //params added for CR 5321

{

    var debitBranchCode = document.fundsShowAccountForm.debitBranchCode.value;

    var creditBranchCode = document.fundsShowAccountForm.creditBranchCode.value;

    var debitAccountNo = document.fundsShowAccountForm.debitAccountNo.value;

    var creditAccountNo = document.fundsShowAccountForm.creditAccountNo.value;

    var debitProdDesc = "";

    var creditProdDesc = "";

    var debitBankSys = "";

    var creditBankSys = "";

    //	Added for CR 1256

    var debitProductCode = "";

    var creditProductCode = "";

    //	End of CR 1256

    if (document.getElementById("dproductDisc") != null)

        debitProdDesc = document.getElementById("dproductDisc").value;

    if (document.getElementById("cproductDisc") != null)

        creditProdDesc = document.getElementById("cproductDisc").value;

    if (document.getElementById("dbankSystem") != null)

        debitBankSys = document.getElementById("dbankSystem").value;

    if (document.getElementById("cbankSystem") != null)

        creditBankSys = document.getElementById("cbankSystem").value;

    //	Added for CR 1256	

    if (document.getElementById("dproductCode") != null)

        debitProductCode = document.getElementById("dproductCode").value;

    if (document.getElementById("cproductCode") != null)

        creditProductCode = document.getElementById("cproductCode").value;

    //	End of CR 1256

    if (creditAccountNo == "") {

        alert("Select loan account");

        return false;

    }

    if (debitAccountNo == "") {

        alert("Select debit account");

        return false;

    }

    if ((debitAccountNo == creditAccountNo) && (debitBranchCode == creditBranchCode)) {

        alert("Cannot transfer to the same account. Choose different accounts");

        return false;

    }

    if (debitBranchCode != creditBranchCode) {

        alert("Due to some technical problems, we are unable to provide inter-branch transfers now. We regret the inconvenience.");

        return false;

    }

    if (!isValidTransfer(debitAccountNo, creditAccountNo, 'Int', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode)) {

        return false;

    }
    fnAlertMobileDetails(bankCode, mobileNo) // CR 5321
    document.fundsShowAccountForm.creditTxnCount.value = "1";

    document.fundsShowAccountForm.submit();

}

function loanClosureValue(accountNo, branchCode, accountType, balance, type) {



    if (type == "loan") {

        document.getElementById("showSelectedCreditAccount").style.display = '';

        document.fundsShowAccountForm.creditAccountNo.value = accountNo;

        document.getElementById("selectedLoanAccount").innerHTML = accountNo;

        document.fundsShowAccountForm.creditBranchCode.value = branchCode;

        document.fundsShowAccountForm.creditAccountType.value = accountType;

        if (balance < 0) {

            balance = balance * -1;

        }

        document.fundsShowAccountForm.amountTransfer.value = balance;

        document.loanClosureViewForm.amountTransfer.value = balance;

    }

    if (type == "debit") {

        document.fundsShowAccountForm.debitAccountNo.value = accountNo;

        document.getElementById("selectedAccount").innerHTML = accountNo;

        document.fundsShowAccountForm.debitBranchCode.value = branchCode;

        document.fundsShowAccountForm.debitAccountType.value = accountType;

    }

}



/********************** DWR *********************/



function loadLocationInfo(locationName) {

    alert("locationName" + locationName);

    var tempArr = new Array();

    tempArr[0] = "--Select Location--";

    DWRUtil.removeAllOptions("locationName");

    DWRUtil.addOptions("locationName", tempArr.concat(locationName));

    tempArr[0] = "--Select Branch--";

    DWRUtil.removeAllOptions("branchName");

    DWRUtil.addOptions("branchName", tempArr);

}



function showLocation() {

    branchDetails.getAllLocation(loadLocationInfo);

}

function showLocationByDWR() {

    branchDetails.getAllLocation({ callback: function (data) { loadLocationInfo(data); } });

}


function loadStateInfo(stateName) {

    var tempArr = new Array();

    tempArr[0] = "--Select State--";

    DWRUtil.removeAllOptions("stateName");

    DWRUtil.addOptions("stateName", tempArr.concat(stateName));

    tempArr[0] = "--Select Location--";

    DWRUtil.removeAllOptions("locationName");

    DWRUtil.addOptions("locationName", tempArr);

    tempArr[0] = "--Select Branch--";

    DWRUtil.removeAllOptions("branchName");

    DWRUtil.addOptions("branchName", tempArr);

}



function showState() {

    branchDetails.getAllState(loadStateInfo);

}

function showStateByDWR() {

    branchDetails.getAllState({ callback: function (data) { loadStateInfo(data); } });

}


function loadLocationInfo(locationName, branchName) {

    var tempArr = new Array();

    tempArr[0] = "--Select Location--";

    DWRUtil.removeAllOptions("locationName");

    if (locationName != null) {

        DWRUtil.addOptions("locationName", tempArr.concat(locationName));

    } else {

        DWRUtil.addOptions("locationName", tempArr);

    }


    tempArr[0] = "--Select Branch--";

    DWRUtil.removeAllOptions("branchName");

    DWRUtil.addOptions("branchName", tempArr);

    if (document.getElementById("locationLoad") != null)

        document.getElementById("locationLoad").style.display = "none";

    document.getElementById("locationName").disabled = false;

    document.getElementById("branchName").disabled = false;

}



function displayLocationPage(page) {

    if (locationLoadFlag == false) {

        locationLoadFlag = true;
        showLocations.forwardLocation(locationForward, page);

    }
}

function displayLocationPageCSS(page) {

    if (locationLoadFlag == false) {

        locationLoadFlag = true;
        //DWR2 IMPL
        showLocations.forwardLocation(page, locationForwardCSS);

    }
}

function locationForwardCSS(data) {

    var innerHtml = DWRUtil.toDescriptiveString(data, 2, { escapeHtml: false });
    innerHtml = innerHtml.substring(1, innerHtml.length - 1).trim().replace(/\\t/g, "");
    document.getElementById('locationLoad').innerHTML = innerHtml;
    //showLocation();

}

function locationForward(data) {


    DWRUtil.setValue("locationLoad", DWRUtil.toDescriptiveString(data, 1));

    //showLocation();

}

//Added for CR-5602 Start ----

function displayBankNamePage(page) {
    if (locationLoadFlag == false) {
        locationLoadFlag = true;
        showLocations.forwardLocation(locationForward1, page);
    }
}

function displayBankNamePageCSS(page) {
    if (locationLoadFlag == false) {
        locationLoadFlag = true;
        //DWR2 IMPL
        showLocations.forwardLocation(page, locationForward1);
    }
}

function locationForward1(data) {
    DWRUtil.setValue("bankNameLoad", DWRUtil.toDescriptiveString(data, 1));
}

function showRTGSState(bankName) {

    document.getElementById("bank45").value = bankName;
    var temp = document.getElementById("bank45").value;


    branchDetails.getRTGSState(loadRTGSStateInfo, bankName);


}

function showRTGSStateByDWR(bankName) {

    document.getElementById("bank45").value = bankName;
    var temp = document.getElementById("bank45").value;

    branchDetails.getRTGSState(bankName, { callback: function (data) { loadRTGSStateInfo(data); } });
}

function showRTGSStateCSS(bankName) {

    document.getElementById("bank45").value = bankName;
    var temp = document.getElementById("bank45").value;

    //DWR2 IMPL
    branchDetails.getRTGSState(bankName, loadRTGSStateInfo);


}

function loadRTGSStateInfo(bankName) {


    var tempArr = new Array();

    tempArr[0] = "--Select State--";

    DWRUtil.removeAllOptions("state");

    if (bankName != null && bankName != "null") {
        DWRUtil.addOptions("state", tempArr.concat(bankName));
    } else {
        DWRUtil.addOptions("state", tempArr);

    }


    //	tempArr[0] = "--Select Branch--";
    //
    //	DWRUtil.removeAllOptions("branchName");
    //
    //	DWRUtil.addOptions("branchName", tempArr);


}

//Added For CR-5602 End---


function showLocation(state) {

    if (locationLoadFlag == false) {

        locationLoadFlag = true;

        branchDetails.getAllLocation(loadLocationInfo, state);

    }

}



function showLocationByDWR(state) {

    if (locationLoadFlag == false) {

        locationLoadFlag = true;

        branchDetails.getAllLocation(state, { callback: function (data) { loadLocationInfo(data); } });

    }

}



function showLocationCSS(state) {

    if (locationLoadFlag == false) {

        locationLoadFlag = true;

        //DWR2 IMPL
        branchDetails.getAllLocation(state, loadLocationInfo);

    }

}


function loadBranchInfo(branchName) {


    var tempArr = new Array();

    tempArr[0] = new Array(2);

    tempArr[0][0] = "--Select Branch--";

    tempArr[0][1] = "--Select Branch--";

    DWRUtil.removeAllOptions("branchName");

    if (branchName != null) {

        DWRUtil.addOptions("branchName", tempArr.concat(branchName), 1, 0);

    } else {

        DWRUtil.addOptions("branchName", tempArr, 1, 0);

    }

}

function showBranch(location, bankCode) {
    branchDetails.getBranchWithCode(loadBranchInfo, location, bankCode);

}
function showBranchByDWR(location, bankCode) {
    branchDetails.getBranchWithCode(location, bankCode, { callback: function (data) { loadBranchInfo(data); } });
}

function showBranchCSS(location, bankCode) {
    //DWR2 IMPL
    branchDetails.getBranchWithCode(location, bankCode, loadBranchInfo);

}

// CR-5602 Start-----
function showBankName(branchName) {


    var location = document.getElementById("bank45").value;

    branchDetails.getBankName(loadINBBankInfo, branchName, location);

}


function showBankNameByDWR(branchName) {
    var location = document.getElementById("bank45").value;

    branchDetails.getBankName(branchName, location, { callback: function (data) { loadINBBankInfo(data); } });
}


function showBankNameCSS(branchName) {


    var location = document.getElementById("bank45").value;

    //DWR2 IMPL
    branchDetails.getBankName(branchName, location, loadINBBankInfo);

}

function loadINBBankInfo(branchName) {



    var tempArr = new Array();

    tempArr[0] = new Array(2);

    //tempArr[0][0] = ""

    tempArr[0][0] = "--Select Branch--";

    tempArr[0][1] = "--Select Branch--";

    DWRUtil.removeAllOptions("branchName");

    if (branchName != null) {

        DWRUtil.addOptions("branchName", tempArr.concat(branchName), 1, 0);

    } else {

        DWRUtil.addOptions("branchName", tempArr, 1, 0);

    }

}
// CR-5602 End -----


function loadINBLocationInfo(locationName) {

    var tempArr = new Array();

    tempArr[0] = "--Select Location--";

    DWRUtil.removeAllOptions("locationName");

    if (locationName != null) {

        DWRUtil.addOptions("locationName", tempArr.concat(locationName));

    } else {

        DWRUtil.addOptions("locationName", tempArr);

    }


    tempArr[0] = "--Select Branch--";

    DWRUtil.removeAllOptions("branchName");

    DWRUtil.addOptions("branchName", tempArr);

}



function showINBLocation(bankCode) {
    inbBranchDetails.getAllLocation(loadINBLocationInfo, bankCode);

}

function showINBLocationCSS(bankCode) {
    //DWR2 IMPL
    inbBranchDetails.getAllLocation(bankCode, loadINBLocationInfo);

}

function showINBLocationByDWR(bankCode) {
    //Added by suresh somepalli for dwr 2.0 		
    inbBranchDetails.getAllLocation(bankCode, { callback: function (data) { loadINBLocationInfo(data); } });
}

function loadINBBranchInfo(branchName) {

    var tempArr = new Array();

    tempArr[0] = new Array(2);

    tempArr[0][0] = "--Select Branch--";

    tempArr[0][1] = "--Select Branch--";

    DWRUtil.removeAllOptions("branchName");

    if (branchName != null) {

        DWRUtil.addOptions("branchName", tempArr.concat(branchName), 1, 0);

    } else {

        DWRUtil.addOptions("branchName", tempArr, 1, 0);

    }

}

function showINBBranch(location, bankCode) {


    inbBranchDetails.getBranchWithCode(loadINBBranchInfo, location, bankCode);

}

function showINBBranchCSS(location, bankCode) {


    //DWR2 IMPL
    inbBranchDetails.getBranchWithCode(location, bankCode, loadINBBranchInfo);

}

function showINBBranchByDWR(location, bankCode) {
    //Added by suresh somepalli for dwr 2.0	
    inbBranchDetails.getBranchWithCode(location, bankCode, { callback: function (data) { loadINBBranchInfo(data); } });
}
/****************** DWR********************/



/********** validation startdate currentdate and enddate(demand draft page) **************/



function compareDates(dateStr1, dateStr2) {



    var datesplit1 = dateStr1.split("/");

    var datesplit2 = dateStr2.split("/");



    var returnValue = true;

    date1 = datesplit1[0];

    month1 = datesplit1[1];

    year1 = datesplit1[2];



    date2 = datesplit2[0];

    month2 = datesplit2[1];

    year2 = datesplit2[2];



    if (parseInt(year1) > parseInt(year2)) {

        returnValue = false;

    } else if (parseInt(year1) == parseInt(year2)) {

        if (parseInt(month1, 10) > parseInt(month2, 10)) {		//sr 88666

            returnValue = false;

        } else if (parseInt(month1, 10) == parseInt(month2, 10)) { //sr 88666

            if (parseInt(date1, 10) > parseInt(date2, 10))  //sr 88666
                returnValue = false;

        }

    }



    return returnValue;

}



function isValidTransfer(strDebitAcc, strCreditAcc, transferType, debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode) {

    var strDebitAccNature;

    var strCreditAccNature;

    var result;

    var strTransferResult;

    if (debitBankSys != "") {

        if (debitBankSys == 'NonCore')

            strDebitAccNature = getAccountNature(strDebitAcc);

        else

            strDebitAccNature = getAccountNatureCoreAssociate(debitProdDesc);


    }

    if (creditBankSys != "") {

        if (creditBankSys == 'NonCore')

            strCreditAccNature = getAccountNature(strCreditAcc);

        else

            strCreditAccNature = getAccountNatureCoreAssociate(creditProdDesc);



    }

    if ((strDebitAccNature == false) || (strCreditAccNature == false)) {

        alert("Invalid funds transfer operation");

        return false;

    }
    //FCNB
    if ((strDebitAccNature == 'RES' && strCreditAccNature == 'FCNB') || (strDebitAccNature == 'FCNB' && strCreditAccNature == 'RES')) {
        strTransferResult = "Yes";
    }
    else {
        strTransferResult = validateTransfer(strDebitAccNature, strCreditAccNature, transferType);
    }
    //FCNB


    if (strTransferResult == "No") {

        alert("You cannot transfer funds from " + strDebitAccNature + " Account to " + strCreditAccNature + " Account.");

        result = false;

    }

    else if (strTransferResult == "Yes") {

        result = true;

    }

    else if (strTransferResult == "Warning") {

        if (confirm("...You are transferring repatriable money into a non repatriable account. This transfer cannot be reversed. Do you want to continue?"))

            result = true;

        else

            result = false;

    }

    return result;

}

function isValidTransferForSBI(strDebitAcc, strCreditAcc, transferType, debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode) {

    var strDebitAccNature;

    var strCreditAccNature;

    var result;

    var strTransferResult; //FCNB

    if (debitBankSys != "") {

        if (debitBankSys == 'NonCore')

            strDebitAccNature = getAccountNature(strDebitAcc);

        else

            strDebitAccNature = getAccountNatureCore(debitProdDesc, debitProductCode, 'D');	//	Passed debitProductCode and 'D' for CR 1256 .D stands for Debit

    }

    if (creditBankSys != "") {

        if (creditBankSys == 'NonCore')

            strCreditAccNature = getAccountNature(strCreditAcc);

        else

            strCreditAccNature = getAccountNatureCore(creditProdDesc, creditProductCode, 'C');	//	Passed creditProductCode and 'C' for CR 1256 .C stands for Credit 


    }
    if ((strDebitAccNature == false) || (strCreditAccNature == false)) {

        alert("Invalid funds transfer operation");

        return false;

    }

    //FCNB
    if ((strDebitAccNature == 'RES' && strCreditAccNature == 'FCNB') || (strDebitAccNature == 'FCNB' && strCreditAccNature == 'RES')) {
        strTransferResult = "Yes";
    }
    else {
        strTransferResult = validateTransfer(strDebitAccNature, strCreditAccNature, transferType);
    }
    //FCNB
    if (strTransferResult == "No") {

        alert("You cannot transfer funds from " + strDebitAccNature + " Account to " + strCreditAccNature + " Account.");

        result = false;

    }

    else if (strTransferResult == "Yes") {

        result = true;

    }

    else if (strTransferResult == "Warning") {

        if (confirm("...You are transferring repatriable money into a non repatriable account. This transfer cannot be reversed. Do you want to continue?"))

            result = true;

        else

            result = false;

    }

    return result;

}


/********** validation startdate,currentdate and enddate(demand draft page) **************/



function getNumerofDaysPerMonth(year, month) {

    var daysofmonth = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    var daysofmonthLY = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    var leapYear = false;

    if ((year / 4) == Math.floor(year / 4))

        leapYear = true;

    if ((year / 400) == Math.floor(year / 400))

        leapYear = true;

    if (leapYear)

        daysofmonth = daysofmonthLY;

    return daysofmonth[month];

}



// CR 897 & 972 --start--on 31-05-06

function submitWithoutPayBillConfirm() {

    document.viewpaywithoutbillForm.submit();

}

function submitFundsTransferConfirm() {


    document.fundsShowAccountForm.submit();

}


//CR 897 & 972 --end--



// Validation for transferfunds.jsp -sbiTodayIntbrLimit added to parametrize the inter branch limit Cr 2957

function submitFundsTransferNew(bankCode, sbiTodayIntbrLimit, txnView) {

    var debitAccountNo = document.fundsShowAccountForm.debitAccountNo.value;

    var creditAccountNo = document.fundsShowAccountForm.creditAccountNo.value;

    var debitBranchCode = document.fundsShowAccountForm.debitBranchCode.value;

    var creditBranchCode = document.fundsShowAccountForm.creditBranchCode.value;

    var amount = document.fundsTransferForm.TransferAmount.value;

    var debitProdDesc = "";

    var creditProdDesc = "";

    var debitBankSys = "";

    var creditBankSys = "";

    //	Added for CR 1256

    var debitProductCode = "";

    var creditProductCode = "";

    //	End of CR 1256

    if (document.getElementById("dproductDisc") != null)

        debitProdDesc = document.getElementById("dproductDisc").value;

    if (document.getElementById("cproductDisc") != null)

        creditProdDesc = document.getElementById("cproductDisc").value;

    if (document.getElementById("dbankSystem") != null)

        debitBankSys = document.getElementById("dbankSystem").value;

    if (document.getElementById("cbankSystem") != null)

        creditBankSys = document.getElementById("cbankSystem").value;

    //	Added for CR 1256	

    if (document.getElementById("dproductCode") != null)

        debitProductCode = document.getElementById("dproductCode").value;

    if (document.getElementById("cproductCode") != null)

        creditProductCode = document.getElementById("cproductCode").value;

    //	End of CR 1256	

    if (!validateFundsTransfer("credit"))

        return false;

    if (bankCode == 0) {

        if (!isValidTransferForSBI(debitAccountNo, creditAccountNo, 'Pri', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode)) {

            return false;

        }

    }

    else {

        if (!isValidTransfer(debitAccountNo, creditAccountNo, 'Pri', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode)) {

            return false;

        }

    }
    //added for retail CR2633 starts


    var creditProductType = document.fundsShowAccountForm.creditAccountType.value;
    if (creditProductType == "B1") {
        //Added for CR 3081
        //commented by sridhar
        /*var PPFLimit = document.getElementById('PPFMiniLimit').value;	
        var PPFMaxLimit = document.getElementById('PPFMaxLimit').value;	
            if(parseFloat(amount)<parseFloat(PPFLimit)){	
                    alert("PPF deposit amount should be a minimum of Rs."+PPFLimit+".");
            return false;
            }*/
        //Added for CR 3081-- end
        //commented by sridhar
        /*if(parseFloat(amount)>parseFloat(PPFMaxLimit))
        {
        alert("The Credit Limit for a PPF Account cannot exceediii "+PPFMaxLimit+"/- ");
        return false;
        }*/
        if (amount % 5 != 0) {
            alert("Amount should be in multiples of 5");
            return false;
        }


    }
    //added for retail CR2633 ends

    // CR 5023 limit is parameterized - 

    if (debitBranchCode != creditBranchCode) {

        if (parseFloat(amount) > parseFloat(sbiTodayIntbrLimit)) // value taken from sbi_name_value_master, name:SBI_TODAY_INTBR_LIMIT_<bankCode>

        {

            alert("You cannot transfer an amount that exceeds " + sbiTodayIntbrLimit + "/- ");

            document.fundsTransferForm.TransferAmount.focus();

            return false;

        }

    }


    //modified for SI schedule starts
    if (creditProductType == "B1") {
        document.fundsShowAccountForm.action = "fundstransferdetails.htm";
    } else {
        document.fundsShowAccountForm.action = "lastfivetxndetails.htm";
    }
    //modified for SI schedule ends	
    //Added For Retail FT TP Scheduling
    if (document.fundsTransferForm.SchedulePay != null) {
        //modified for SI schedule starts
        if (document.fundsTransferForm.SchedulePay[1].checked || (document.fundsTransferForm.SchedulePay[2] != null && document.fundsTransferForm.SchedulePay[2].checked)) {
            var schDate = document.getElementById("Days").value + "/" + document.getElementById("Months").value + "/" + document.getElementById("Years").value;
            // Modified for CR-5609
            if (isERDAccount()) {

                schDate = document.getElementById("erdDays").value + "/" + document.getElementById("erdMonths").value + "/" + document.getElementById("erdYears").value;

            }
            // Modified for CR-5609
            document.getElementById("schPayDate").value = schDate;
        }
        if (document.fundsTransferForm.SchedulePay[2] != null && document.fundsTransferForm.SchedulePay[2].checked) {
            var freqType = document.getElementById("freqType").value;
            var freqInDays = document.getElementById("freqInDays").value;
            var noOfPayments = document.getElementById("noOfPayments").value;
            var accType = eval(document.getElementById("creditAccountType")).value; /* Added for SI PPF account - martin */
            if (!purenumeric('fundsTransferForm', 'noOfPayments', 'Number Of Payments')) {
                return false;
            } else if (eval(noOfPayments) < 2) {
                alert("Number of payments should exceed 1");
                return false;
            }
            /* Added for SI PPF account - martin */
            if (accType == 'B1' && eval(noOfPayments) > 12) {
                alert("Number of payments should not exceed 12");
                return false;
            }
            /* Added for SI PPF account - martin*/

            // Added for CR 5609 
            if (isERDAccount() && eval(noOfPayments) > 119) {
                alert("Number of payments should not exceed ten years from the e-RD A/c creation date.");
                return false;
            }
            // Added for CR 5609

            var yearInDays = 365 * 24 * 60 * 60 * 1000;
            var datesplit1 = (document.getElementById("schPayDate").value).split("/");
            date = datesplit1[0];
            month = datesplit1[1];
            var monArr = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
            for (var i = 0; i < 12; i++) {
                if (monArr[i] == month) {
                    month = i;
                    break;
                }
            }
            year = datesplit1[2];
            var startDate = new Date(year, month, date);
            var startTime = startDate.getTime();
            var dateoneYearAfter = new Date(startTime + yearInDays);
            var daysAdded = null;
            if (freqType == 'Days') {
                daysAdded = eval(freqInDays) * eval(noOfPayments);
            } else {
                daysAdded = eval(freqInDays) * 30 * eval(noOfPayments);
            }
            var daysAddedInTime = eval(daysAdded) * 24 * 60 * 60 * 1000;
            lastPayDate = new Date(daysAddedInTime + startTime);
            // Modified for CR : 5609 - Parameshwaran
            if (lastPayDate > dateoneYearAfter && !isERDAccount()) {
                alert("Period for standing instruction should not exceed one year");
                return false;
            }

            // Financial Year Calculation
            if (creditProductType == "B1") {
                var schDate = document.getElementById("Days").value + "/" + document.getElementById("Months").value + "/" + document.getElementById("Years").value;
                document.getElementById("schPayDate").value = schDate;

                if (!validatePPFFreq(parseFloat(amount))) {
                    return false;
                }
            }

            // Modified for CR : 5609 - Parameshwaran
            document.fundsShowAccountForm.siFreqType.value = freqType;
            document.fundsShowAccountForm.siFreqInDays.value = freqInDays;
            document.fundsShowAccountForm.siNoOfPayments.value = noOfPayments;
            document.fundsShowAccountForm.action = "lastfivesitransactions.htm";
        }
        //modified for SI schedule ends
    }
    //Added for CR 5015

    if (txnView == 'showLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'showLastFiveTransactions';
    }
    if (txnView == 'showSILastFiveTxn') {
        document.getElementById('lastFiveSITransactionFlag').value = 'showLastFiveSITransactions';
    }
    if (txnView == 'noShowLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'noShowLastFiveTransactions';
        document.getElementById('lastFiveSITransactionFlag').value = 'noShowLastFiveSITransactions';
    }
    if (creditProductType == "B1") {
        if (txnView == 'showLastFiveTxn')
            alert("If the credit account is a PPF account, the last five transaction details cannot be displayed here.");
    }
    //End of CR 5015

    document.fundsShowAccountForm.creditTxnCount.value = "1";

    document.fundsShowAccountForm.amountTransfer.value = document.fundsTransferForm.TransferAmount.value;

    document.fundsShowAccountForm.transactionRemarks.value = document.fundsTransferForm.transactionRemarks.value;

    document.fundsShowAccountForm.debitProductCode.value = debitProductCode;	//	Added for CR 1256

    document.fundsShowAccountForm.creditProductCode.value = creditProductCode;	//	Added for CR 1256	




    document.fundsShowAccountForm.submit();

}

//validation for eztradetransfer.jsp added CR 2957 - 
function submitEzTradeTransfer(bankCode, ezTradeInterBrLimit, ezTradeIntraBrLimit, txnView) {

    var debitAccountNo = document.fundsShowAccountForm.debitAccountNo.value;

    var creditAccountNo = document.fundsShowAccountForm.creditAccountNo.value;

    var debitBranchCode = document.fundsShowAccountForm.debitBranchCode.value;

    var creditBranchCode = document.fundsShowAccountForm.creditBranchCode.value;

    var amount = document.fundsTransferForm.TransferAmount.value;

    var debitProdDesc = "";

    var creditProdDesc = "";

    var debitBankSys = "";

    var creditBankSys = "";

    var debitProductCode = "";

    var creditProductCode = "";

    if (document.getElementById("dproductDisc") != null)

        debitProdDesc = document.getElementById("dproductDisc").value;

    if (document.getElementById("cproductDisc") != null)

        creditProdDesc = document.getElementById("cproductDisc").value;

    if (document.getElementById("dbankSystem") != null)

        debitBankSys = document.getElementById("dbankSystem").value;

    if (document.getElementById("cbankSystem") != null)

        creditBankSys = document.getElementById("cbankSystem").value;



    if (document.getElementById("dproductCode") != null)

        debitProductCode = document.getElementById("dproductCode").value;

    if (document.getElementById("cproductCode") != null)

        creditProductCode = document.getElementById("cproductCode").value;




    if (!validateFundsTransfer("credit"))

        return false;


    if (!ezTradeNRENROValidation(debitProdDesc, creditProdDesc)) {
        alert("Invalid eZtrade funds transfer.Please select valid parent-child combination");
        return false;
    }

    if (bankCode == 0) {

        if (!isValidTransferForSBI(debitAccountNo, creditAccountNo, 'Pri', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode)) {

            return false;

        }

    }

    else {

        if (!isValidTransfer(debitAccountNo, creditAccountNo, 'Pri', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode)) {

            return false;

        }

    }


    if (debitBranchCode != creditBranchCode) {
        if (parseInt(amount) > ezTradeInterBrLimit) {
            alert("transfer amount exceeds the limit Rs." + ezTradeInterBrLimit + "/- specified by bank");
            document.fundsTransferForm.amountTransfer.focus();
            return false;
        }
    } else {
        if (parseInt(amount) > ezTradeIntraBrLimit) {
            alert("transfer amount exceeds the limit Rs." + ezTradeIntraBrLimit + "/- specified by bank");
            document.fundsTransferForm.amountTransfer.focus();
            return false;
        }
    }
    //Added for CR 5015	
    if (txnView == 'showLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'showLastFiveTransactions';
    }
    if (txnView == 'noShowLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'noShowLastFiveTransactions';
    }
    //End of CR 5015
    document.fundsShowAccountForm.creditTxnCount.value = "1";

    document.fundsShowAccountForm.amountTransfer.value = document.fundsTransferForm.TransferAmount.value;

    document.fundsShowAccountForm.transactionRemarks.value = document.fundsTransferForm.transactionRemarks.value;

    document.fundsShowAccountForm.debitProductCode.value = debitProductCode;

    document.fundsShowAccountForm.creditProductCode.value = creditProductCode;



    document.fundsShowAccountForm.action = "lastfivetxndetails.htm";

    document.fundsShowAccountForm.submit();
}
//eZ trade transfer end 

//Added for RTGS

function submitInterBankTransfer(bankCode) {

    var debitAccountNo = document.fundsShowAccountForm.debitAccountNo.value;

    var creditAccountNo = document.fundsShowAccountForm.creditAccountNo.value;

    var debitBranchCode = document.fundsShowAccountForm.debitBranchCode.value;

    var creditBranchCode = document.fundsShowAccountForm.creditBranchCode.value;

    var amount = document.fundsTransferForm.TransferAmount.value;;

    var thirdPartyLimit = document.fundsShowAccountForm.thirdparty_limit.value;

    var accountLimit = document.fundsShowAccountForm.accountLimit.value;

    var transactionType = document.fundsShowAccountForm.transactionType.value;

    var debitProdDesc = "";

    var creditProdDesc = "";

    var debitBankSys = "";

    var creditBankSys = "";

    if (!validateFundsTransfer('credit'))

        return false;



    if (parseFloat(amount) > parseFloat(accountLimit)) {

        alert("The transfer amount for selected beneficiary cannot exceed Rs." + accountLimit);

        document.fundsTransferForm.TransferAmount.focus();

        return false;

    }

    if (transactionType == 'RTGS') {

        if (parseFloat(amount) < 200000) {

            alert("RTGS transactions should not be less than 2 lakh");

            document.fundsTransferForm.TransferAmount.focus();

            return false;

        }

    }

    document.fundsShowAccountForm.creditTxnCount.value = "1";

    document.fundsShowAccountForm.amountTransfer.value = document.fundsTransferForm.TransferAmount.value;

    document.fundsShowAccountForm.transactionRemarks.value = document.fundsTransferForm.transactionRemarks.value;

    if (document.fundsTransferForm.acceptTerms.checked == false) {

        alert("Please read and accept the terms and conditions before proceeding");

        return false;

    }

    document.fundsShowAccountForm.submit();

}



function setCreditBankBranch(bankName, branchName, ifscCode) {

    document.fundsShowAccountForm.creditBankName.value = bankName;

    document.fundsShowAccountForm.creditBranchName.value = branchName;

    document.fundsShowAccountForm.ifscCode.value = ifscCode;

}

//Added for Make Donation Link
function submitMakeDonation(bankCode, txnView) {

    var debitAccountNo = document.fundsShowAccountForm.debitAccountNo.value;

    var creditAccountNo = document.fundsShowAccountForm.creditAccountNo.value;

    var debitBranchCode = document.fundsShowAccountForm.debitBranchCode.value;

    var creditBranchCode = document.fundsShowAccountForm.creditBranchCode.value;

    var amount = document.fundsTransferForm.TransferAmount.value;

    var donorName = document.fundsTransferForm.donorName.value;


    var streetName = trimAll(document.fundsTransferForm.streetName.value);

    var city = trimAll(document.fundsTransferForm.city.value);

    var state = trimAll(document.fundsTransferForm.state.value);
    var country = trimAll(document.fundsTransferForm.country.value);

    var pinCode = trimAll(document.fundsTransferForm.pinCode.value);



    if (!alphanum(donorName, "Donor Name")) {
        document.fundsTransferForm.streetName.focus();
        return false;
    }


    if (!alphanum(streetName, "Street Name")) {
        document.fundsTransferForm.streetName.focus();
        return false;
    }



    if (!alphanum(city, "City")) {
        document.fundsTransferForm.city.focus();
        return false;
    }



    if (!alphanum(state, "State")) {
        document.fundsTransferForm.state.focus();
        return false;
    }
    if (!alphanum(state, "Country")) {
        document.fundsTransferForm.country.focus();
        return false;
    }
    /*
    if(purenumeric('fundsTransferForm','pinCode','PIN Code')){
        document.fundsTransferForm.pinCode.focus();
        return false;
    }
    */


    var debitProdDesc = "";

    var debitBankSys = "";

    var creditBankSys = "";

    var debitProductCode = "";

    var maxLimit = document.fundsShowAccountForm.maxLimit.value;

    var EHSLimit = document.fundsShowAccountForm.EHSLimit.value;

    var charityName = document.fundsShowAccountForm.charityName.value;

    var donorName = document.fundsTransferForm.donorName.value;

    var smsSecurityValue = document.fundsShowAccountForm.smsSecurity.value;

    if (document.getElementById("dproductDisc") != null)

        debitProdDesc = document.getElementById("dproductDisc").value;

    if (document.getElementById("dbankSystem") != null)

        debitBankSys = document.getElementById("dbankSystem").value;

    if (document.getElementById("cbankSystem") != null)

        creditBankSys = document.getElementById("cbankSystem").value;


    if (document.getElementById("dproductCode") != null)

        debitProductCode = document.getElementById("dproductCode").value;



    if (donorName == "") {
        alert("Please enter Donor name.");
        document.fundsTransferForm.donorName.focus();
        return false;
    }


    if (streetName == "") {
        alert("Please enter address1.");
        document.fundsTransferForm.streetName.focus();
        return false;

    }

    if (city == "") {
        alert("Please enter city name.");
        document.fundsTransferForm.city.focus();
        return false;
    }

    if (state == "") {
        alert("Please enter state name.");
        document.fundsTransferForm.state.focus();
        return false;
    }
    if (country == "") {
        alert("Please enter country name.");
        document.fundsTransferForm.country.focus();
        return false;
    }


    if (pinCode == "") {
        alert("Please enter PIN Code.");
        document.fundsTransferForm.pinCode.focus();
        return false;
    }
    if (isNaN(pinCode) == true) {
        alert("Please enter numeric only for PIN Code.");
        document.fundsTransferForm.pinCode.focus();
        return false;

    }

    if (debitAccountNo == "") {
        alert("Please select debit account");
        return false;
    }
    if (amount == "") {
        alert("Please enter amount");
        document.fundsTransferForm.TransferAmount.focus();
        return false;
    }
    if (charityName == "" || creditAccountNo == "") {
        alert("Please select a Charity");
        return false;
    }


    if (!validateFundsTransfer("credit"))
        return false;

    if (parseFloat(amount) > parseFloat(maxLimit)) {
        alert("Amount exceeds the Maximum Limit for this Charity");
        document.fundsTransferForm.TransferAmount.focus();
        return false;

    }

    if ((parseFloat(amount) > parseFloat(EHSLimit)) && smsSecurityValue != "0") {
        var check = confirm("Amount exceeds the EHS Limit allowed for this Charity. To continue you must enable High Security settings. Proceed?");
        if (check) {
            document.fundsShowAccountForm.action = "paymentenablehighsecurity.htm";
            document.fundsShowAccountForm.submit();
            return false;
        }
        else {
            document.fundsTransferForm.TransferAmount.focus();
            return false;
        }

    }

    //Added for CR 5015
    if (txnView == 'showLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'showLastFiveTransactions';
    }
    if (txnView == 'noShowLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'noShowLastFiveTransactions';
    }
    //End of CR 5015

    //Added for CR 5157 Starts
    document.fundsShowAccountForm.streetName.value = document.fundsTransferForm.streetName.value;

    document.fundsShowAccountForm.city.value = document.fundsTransferForm.city.value;

    document.fundsShowAccountForm.state.value = document.fundsTransferForm.state.value;

    document.fundsShowAccountForm.pinCode.value = document.fundsTransferForm.pinCode.value;
    //Added for CR 5157 Ends

    document.fundsShowAccountForm.creditTxnCount.value = "1";

    document.fundsShowAccountForm.amountTransfer.value = document.fundsTransferForm.TransferAmount.value;

    document.fundsShowAccountForm.transactionRemarks.value = document.fundsTransferForm.transactionRemarks.value;

    document.fundsShowAccountForm.debitProductCode.value = debitProductCode;

    document.fundsShowAccountForm.transactionRemarks.value = document.fundsTransferForm.transactionRemarks.value;

    if (trimAll(donorName) != null && trimAll(donorName) != "")

        document.fundsShowAccountForm.donorName.value = document.fundsTransferForm.donorName.value;

    else

        document.fundsShowAccountForm.donorName.value = document.fundsShowAccountForm.userFriendlyName.value;


    document.fundsShowAccountForm.submit();

}



function alphanum(field, message) {
    var specialChar = "`=[]\\;'~!@$%^&*()+{}|:\"<>?";
    var space = " ";

    for (i = 0; i < (field.length); i++) {
        if (specialChar.indexOf(field.substring(i, i + 1)) != '-1') {
            alert("Special characters not allowed in " + message);
            return false;
        }
    }
    return true;
}

function checkDonationAccountDetails(charityID, charityName, accountNo, branchCode, type, maxLimit, EHSLimit) {
    if (type == "credit") {

        document.getElementById('showSelc').style.display = '';

        document.fundsShowAccountForm.creditAccountNo.value = accountNo;

        document.fundsShowAccountForm.charityName.value = charityName;

        document.fundsShowAccountForm.charityID.value = charityID;

        document.getElementById("selc").innerHTML = charityName;

        document.fundsShowAccountForm.creditBranchCode.value = branchCode;

        document.fundsShowAccountForm.maxLimit.value = maxLimit;

        document.fundsShowAccountForm.EHSLimit.value = EHSLimit;


    }


}

function trimAll(sString) {

    while (sString.substring(0, 1) == ' ') {
        sString = sString.substring(1, sString.length);
    }
    while (sString.substring(sString.length - 1, sString.length) == ' ') {
        sString = sString.substring(0, sString.length - 1);
    }
    return sString;
}
//Make Donation Link - end
// NRENROValidation CR 2957-start
function ezTradeNRENROValidation(debitProdDesc, creditProdDesc) {
    if (debitProdDesc.search('NRE') != -1) {
        if (creditProdDesc.search('NRE') != -1) {

            return true;
        }
        else {
            return false;
        }
    }
    else if (debitProdDesc.search('NRO') != -1) {
        if (creditProdDesc.search('NRO') != -1) {

            return true;
        }
        else {
            return false;
        }
    }
    else return false;
}

// NRENROValidation -end


//Added for CR 5039

function showCloseAccounts() {
    document.viewAllAccountForm.action = 'viewClosedAccounts.htm';
    document.viewAllAccountForm.accountStatementDisplay.value = 'accountSummary';
    document.viewAllAccountForm.submit();
}


function submitClosedAccountForm(accountNo, branchCode) {

    document.quickLookForm.accountNo.value = accountNo;

    document.quickLookForm.branchCode.value = branchCode;

    document.quickLookForm.closedAccount.value = 'CA';

    document.quickLookForm.submit();
}
//Added for CR 5039 ends

//Added for CR 5218
function six_months_validation() {
    var month = new Array(12);

    var start_date = document.getElementById("startdate").value;
    var end_date = document.getElementById("enddate").value;

    var datesplit1 = start_date.split("/");
    date1 = datesplit1[0];
    month1 = datesplit1[1];
    year1 = datesplit1[2];

    var datesplit2 = end_date.split("/");
    date2 = datesplit2[0];
    month2 = datesplit2[1];
    year2 = datesplit2[2];

    if (year2 % 4 == '0') {
        month[1] = '31';
        month[2] = '29';
        month[3] = '31';
        month[4] = '30';
        month[5] = '31';
        month[6] = '30';
        month[7] = '31';
        month[8] = '31';
        month[9] = '30';
        month[10] = '31';
        month[11] = '30';
        month[12] = '31';
    }
    else {
        month[1] = '31';
        month[2] = '28';

        month[3] = '31';
        month[4] = '30';
        month[5] = '31';
        month[6] = '30';
        month[7] = '31';
        month[8] = '31';
        month[9] = '30';
        month[10] = '31';
        month[11] = '30';
        month[12] = '31';
    }

    if (date2.toString().length == '1')
        date2 = "0" + date2;
    if (month2.toString().length == '1')
        month2 = "0" + month2;


    if (parseInt(year1) == year2) {
        var a = eval(month1);
        var c = eval(month2) - a;
        if (c == 6) {
            if (parseInt(date1) > date2) {
                return true;
            } else {
                alert("Date range cannot exceed 6 months");
                return false;
            }
        } else if (c < 6) {
            return true;
        } else {
            alert("Date range cannot exceed 6 months");
            return false;
        }
    }
    else if (parseInt(year1) == parseInt(year2) - 1) {
        var a = eval(month1);
        var b = 12 - a;
        var c = b + eval(month2);
        if (c == 6) {
            if (parseInt(date1) > date2) {
                return true;
            } else {
                alert("Date range cannot exceed 6 months");
                return false;
            }
        } else if (c < 6) {
            return true;
        } else {
            alert("Date range cannot exceed 6 months");
            return false;
        }
    } else {
        alert("Date range cannot exceed 6 months");
        return false;
    }
    return true;
}
//End of CR 5218
function six_months_validation_new() {
    var month = new Array(12);

    var start_date = document.getElementById("datepicker1").value;
    var end_date = document.getElementById("datepicker2").value;

    var datesplit1 = start_date.split("/");
    date1 = datesplit1[0];
    month1 = datesplit1[1];
    year1 = datesplit1[2];

    var datesplit2 = end_date.split("/");
    date2 = datesplit2[0];
    month2 = datesplit2[1];
    year2 = datesplit2[2];

    if (year2 % 4 == '0') {
        month[1] = '31';
        month[2] = '29';
        month[3] = '31';
        month[4] = '30';
        month[5] = '31';
        month[6] = '30';
        month[7] = '31';
        month[8] = '31';
        month[9] = '30';
        month[10] = '31';
        month[11] = '30';
        month[12] = '31';
    }
    else {
        month[1] = '31';
        month[2] = '28';
        month[3] = '31';
        month[4] = '30';
        month[5] = '31';
        month[6] = '30';
        month[7] = '31';
        month[8] = '31';
        month[9] = '30';
        month[10] = '31';
        month[11] = '30';
        month[12] = '31';
    }

    if (date2.toString().length == '1')
        date2 = "0" + date2;
    if (month2.toString().length == '1')
        month2 = "0" + month2;


    if (parseInt(year1) == year2) {
        var a = eval(month1);
        var c = eval(month2) - a;
        if (c == 6) {
            if (parseInt(date1) > date2) {
                return true;
            } else {
                alert("Date range cannot exceed 6 months");
                return false;
            }
        } else if (c < 6) {
            return true;
        } else {
            alert("Date range cannot exceed 6 months");
            return false;
        }
    }
    else if (parseInt(year1) == parseInt(year2) - 1) {
        var a = eval(month1);
        var b = 12 - a;
        var c = b + eval(month2);
        if (c == 6) {
            if (parseInt(date1) > date2) {
                return true;
            } else {
                alert("Date range cannot exceed 6 months");
                return false;
            }
        } else if (c < 6) {
            return true;
        } else {
            alert("Date range cannot exceed 6 months");
            return false;
        }
    } else {
        alert("Date range cannot exceed 6 months");
        return false;
    }
    return true;
}
//CR 5321
function fnAlertMobileDetails(bankCode, mobileNo) {
    ////		if (bankCode != null && bankCode.length>0 && (bankCode == "0" || bankCode == "6" ||bankCode == "A" ))
    //	if (bankCode != null && bankCode.length>0)
    //{
    if (mobileNo == null || mobileNo.length <= 0) {
        alert("Currently your mobile number is not registered in the site. Please register your mobile number in the Profile Personal Details section to avail better services.");

    }
    //}
    return true;
}



function validatePPFFreq(amount) {

    var noOfPymts = document.getElementById('noOfPayments').value;
    var freqType = document.getElementById('freqInDays').value; //Payment every
    var PPFMaxLimit = document.getElementById('PPFMaxLimit').value;
    //var amount = parseFloat(document.fundsTransferForm.TransferAmount.value);

    var ppfdatesplit1 = (document.getElementById("schPayDate").value).split("/");
    ppfdate = ppfdatesplit1[0];
    ppfmonth = ppfdatesplit1[1];
    var monArr = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
    for (var i = 0; i < 12; i++) {
        if (monArr[i] == ppfmonth) {
            ppfmonth = i;
            break;
        }
    }
    ppfyear = ppfdatesplit1[2];

    var stDt = new Date(ppfyear, ppfmonth, ppfdate);  // Input date get from the UI 		
    /*var dt=parseInt(stDt.getDate()+1);		
    stDt.setDate(dt); */ // Next day		
    var endDt = new financeYearEndDate(); // calculated from server date
    var fymonthsleft = dateDifference(endDt, stDt, 'months') + 1; // valid months in current financial year

    var fyPayments = 1; // valid payments for this FY
    var remainingPymts = 0; // valid payments for next FY
    if (fymonthsleft > 0) {
        // Exclude current payment date ( noOfPymts -1 )
        for (i = freqType; i <= ((noOfPymts - 1) * freqType); i = parseInt(i) + parseInt(freqType)) {
            var nextDt = new Date(stDt.getTime());
            //nextDt.setMonth((nextDt.getMonth()+parseInt(i)+1)));
            var addMon = parseInt(i);
            nextDt.setMonth(nextDt.getMonth() + addMon);
            if (dateDifference(nextDt, endDt, 'days') >= 0) {
                fyPayments++;
            } else {
                remainingPymts++;
            }


        }
        //commnetd by sridhar      
        /* if( (fyPayments*amount > parseInt(PPFMaxLimit)) ){
             alert("The cumulative value of standing instructions for the current financial year should not exceed  "+PPFMaxLimit+"/-");
             return false;
         }
         if((remainingPymts*amount > parseInt(PPFMaxLimit)) ){
             alert("The cumulative value of standing instructions  should not exceed  "+PPFMaxLimit+"/- for the next financial year.");
             return false;
         }*/
    }
    return true;
}

// Added for CR :5609
function isERDAccount() {
    try {
        var creAccType = document.getElementById('creditAccountType').value;
        var prdDesc = document.getElementById('cproductDisc').value;
        var bankCode = document.getElementById('bankCode').value
        var chkBoxId = getCheckedObjectId(document.fundsTransferForm.c); // Added for branch rd defect			
        var isSIChecked = (getCheckedObjectValue(document.fundsTransferForm.SchedulePay) == "scheduleSI");
        var brRDAcc = document.getElementById(chkBoxId).getAttribute("branchRDAcc");
        var isBranchRD = (brRDAcc == "BRANCH_RD");
        document.getElementById('branchRDAcc').value = brRDAcc;
        if (creAccType == 'A5' && (prdDesc.indexOf("RD") == 0 || prdDesc.indexOf("FLEXI") == 0) && isSIChecked && !isBranchRD) {
            document.getElementById('nonERDText').style.display = "none";
            document.getElementById('erdText').style.display = "";
            document.getElementById('freqPymtRow').style.display = "none";
            return true;
        }
        document.getElementById('nonERDText').style.display = "";
        document.getElementById('erdText').style.display = "none";
        document.getElementById('freqPymtRow').style.display = "";
    } catch (e) { }
    return false;
}


/*landing page acc balance start */

function getAccBal(account_no, branchCode, userName) {
    document.getElementById('accBal' + account_no).style.textDecoration = "none";
    quickAccountBalance.getAccountBalance(getAccBalance, account_no, branchCode, userName);
}

function getAccBalCSS(account_no, branchCode, userName) {
    document.getElementById('accBal' + account_no).style.textDecoration = "none";
    //DWR2 IMPL
    quickAccountBalance.getAccountBalance(account_no, branchCode, userName, getAccBalance);
}

function getAccBalance(accountData) {
    if (accountData != null) {
        if (accountData.sessionIsNull == 'YES') {
            document.viewAllAccountForm.action = "sessiontimeout.htm";
            document.viewAllAccountForm.submit();
        }
        else {
            var account_no = accountData.account_no;
            var accBalance = accountData.accBalance;
            var accstatus = accountData.account_status;
            var currency_code = accountData.currency_code;
            var data = currency_code + "  " + accBalance;

            if (accBalance != "Please Try Later") {
                if (accstatus != null && (accstatus == 'CLOS' || accstatus == 'DISC')) {
                    alert("The account seems to be 'Closed' as per bank's record, it will be reflected in closed account list");
                }
                document.getElementById('accBal' + account_no).style.display = "none";
                document.getElementById('accBalRes' + account_no).style.display = "";
                document.getElementById('accBalRes' + account_no).innerHTML = data;
            }
            else {
                document.getElementById('accBal' + account_no).innerHTML = accBalance;
            }
        }
    }
}


function submitQuickLookFormValidation(account_no, branchCode, userName) {
    quickAccountBalance.getAccountBalanceForLastTenTxn(quickLookFormValidation, account_no, branchCode, userName);
}

function submitQuickLookFormValidationCSS(account_no, branchCode, userName) {
    //DWR2 IMPL
    quickAccountBalance.getAccountBalanceForLastTenTxn(account_no, branchCode, userName, quickLookFormValidation);
}

function quickLookFormValidation(accountData) {
    if (accountData != null) {
        if (accountData.sessionIsNull == 'YES') {
            document.viewAllAccountForm.action = "sessiontimeout.htm";
            document.viewAllAccountForm.submit();
        }
        else {
            var account_no = accountData.account_no;
            var branchCode = accountData.branchCode;
            var accstatus = accountData.account_status;
            if (quickLookForm) {
                quickLookForm = false;
                if (accstatus != null && (accstatus == 'CLOS' || accstatus == 'DISC')) {
                    alert("The account seems to be 'Closed' as per bank's record, it will be reflected in closed account list");
                }
                document.quickLookForm.accountNo.value = account_no;
                document.quickLookForm.branchCode.value = branchCode;
                document.quickLookForm.submit();
            } else {
                alert("Previous 'Request' is inprogress");
            }
        }
    }
}

/* Added for Transaction Accounts*/


/*landing page acc balance end */


function submitFundsTransfers(bankCode, sbiTodayIntbrLimit, txnView) {
    var debitAccountNo = document.fundsShowAccountForm.debitAccountNo.value;

    var creditAccountNo = document.fundsShowAccountForm.creditAccountNo.value;

    var debitBranchCode = document.fundsShowAccountForm.debitBranchCode.value;

    var creditBranchCode = document.fundsShowAccountForm.creditBranchCode.value;

    var amount = document.fundsTransferForm.TransferAmount.value;

    var debitProdDesc = "";

    var creditProdDesc = "";

    var debitBankSys = "";

    var creditBankSys = "";

    //	Added for CR 1256

    var debitProductCode = "";

    var creditProductCode = "";

    //	End of CR 1256

    if (document.getElementById("dproductDisc") != null)

        debitProdDesc = document.getElementById("dproductDisc").value;

    if (document.getElementById("cproductDisc") != null)

        creditProdDesc = document.getElementById("cproductDisc").value;

    if (document.getElementById("dbankSystem") != null)

        debitBankSys = document.getElementById("dbankSystem").value;

    if (document.getElementById("cbankSystem") != null)

        creditBankSys = document.getElementById("cbankSystem").value;

    //	Added for CR 1256	

    if (document.getElementById("dproductCode") != null)

        debitProductCode = document.getElementById("dproductCode").value;

    if (document.getElementById("cproductCode") != null)

        creditProductCode = document.getElementById("cproductCode").value;

    //	End of CR 1256	

    if (!validateFundsTransfer("credit"))

        return false;

    if (bankCode == 0) {

        if (!isValidTransferForSBI(debitAccountNo, creditAccountNo, 'Pri', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode)) {

            return false;

        }

    }

    else {

        if (!isValidTransfer(debitAccountNo, creditAccountNo, 'Pri', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode)) {

            return false;

        }

    }
    //added for retail CR2633 starts


    var creditProductType = document.fundsShowAccountForm.creditAccountType.value;
    if (creditProductType == "B1") {
        //Added for CR 3081
        //commented by sridhar
        /*var PPFLimit = document.getElementById('PPFMiniLimit').value;	
        var PPFMaxLimit = document.getElementById('PPFMaxLimit').value;	
            if(parseFloat(amount)<parseFloat(PPFLimit)){	
                    alert("PPF deposit amount should be a minimum of Rs."+PPFLimit+".");
            return false;
            }*/
        //Added for CR 3081-- end
        //commented by sridhar
        /*if(parseFloat(amount)>parseFloat(PPFMaxLimit))
        {
        alert("The Credit Limit for a PPF Account cannot exceediii "+PPFMaxLimit+"/- ");
        return false;
        }*/
        if (amount % 5 != 0) {
            alert("Amount should be in multiples of 5");
            return false;
        }


    }
    //added for retail CR2633 ends

    // CR 5023 limit is parameterized - 

    if (debitBranchCode != creditBranchCode) {

        if (parseFloat(amount) > parseFloat(sbiTodayIntbrLimit)) // value taken from sbi_name_value_master, name:SBI_TODAY_INTBR_LIMIT_<bankCode>

        {

            alert("You cannot transfer an amount that exceeds " + sbiTodayIntbrLimit + "/- ");

            document.fundsTransferForm.TransferAmount.focus();

            return false;

        }

    }


    //modified for SI schedule starts
    if (creditProductType == "B1") {
        document.fundsShowAccountForm.action = "fundstransferdetails.htm";
    } else {
        document.fundsShowAccountForm.action = "lastfivetxndetails.htm";
    }
    //modified for SI schedule ends	
    //Added For Retail FT TP Scheduling
    if (document.fundsTransferForm.SchedulePay != null) {
        //modified for SI schedule starts
        if (document.fundsTransferForm.SchedulePay[1].checked || (document.fundsTransferForm.SchedulePay[2] != null && document.fundsTransferForm.SchedulePay[2].checked)) {
            var schDate = document.getElementById("Days").value + "/" + document.getElementById("Months").value + "/" + document.getElementById("Years").value;
            // Modified for CR-5609
            if (isERDAccount()) {

                schDate = document.getElementById("erdDays").value + "/" + document.getElementById("erdMonths").value + "/" + document.getElementById("erdYears").value;

            }
            // Modified for CR-5609
            document.getElementById("schPayDate").value = schDate;
        }
        if (document.fundsTransferForm.SchedulePay[2] != null && document.fundsTransferForm.SchedulePay[2].checked) {
            var freqType = document.getElementById("freqType").value;
            var freqInDays = document.getElementById("freqInDays").value;
            var noOfPayments = document.getElementById("noOfPayments").value;
            var accType = eval(document.getElementById("creditAccountType")).value; /* Added for SI PPF account - martin */
            if (!purenumeric('fundsTransferForm', 'noOfPayments', 'Number Of Payments')) {
                return false;
            } else if (eval(noOfPayments) < 2) {
                alert("Number of payments should exceed 1");
                return false;
            }
            /* Added for SI PPF account - martin */
            if (accType == 'B1' && eval(noOfPayments) > 12) {
                alert("Number of payments should not exceed 12");
                return false;
            }
            /* Added for SI PPF account - martin*/

            // Added for CR 5609 
            if (isERDAccount() && eval(noOfPayments) > 119) {
                alert("Number of payments should not exceed ten years from the e-RD A/c creation date.");
                return false;
            }
            // Added for CR 5609

            var yearInDays = 365 * 24 * 60 * 60 * 1000;
            var datesplit1 = (document.getElementById("schPayDate").value).split("/");
            date = datesplit1[0];
            month = datesplit1[1];
            var monArr = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
            for (var i = 0; i < 12; i++) {
                if (monArr[i] == month) {
                    month = i;
                    break;
                }
            }
            year = datesplit1[2];
            var startDate = new Date(year, month, date);
            var startTime = startDate.getTime();
            var dateoneYearAfter = new Date(startTime + yearInDays);
            var daysAdded = null;
            if (freqType == 'Days') {
                daysAdded = eval(freqInDays) * eval(noOfPayments);
            } else {
                daysAdded = eval(freqInDays) * 30 * eval(noOfPayments);
            }
            var daysAddedInTime = eval(daysAdded) * 24 * 60 * 60 * 1000;
            lastPayDate = new Date(daysAddedInTime + startTime);
            // Modified for CR : 5609 - Parameshwaran
            if (lastPayDate > dateoneYearAfter && !isERDAccount()) {
                alert("Period for standing instruction should not exceed one year");
                return false;
            }

            // Financial Year Calculation
            if (creditProductType == "B1") {
                var schDate = document.getElementById("Days").value + "/" + document.getElementById("Months").value + "/" + document.getElementById("Years").value;
                document.getElementById("schPayDate").value = schDate;

                if (!validatePPFFreq(parseFloat(amount))) {
                    return false;
                }
            }

            // Modified for CR : 5609 - Parameshwaran
            document.fundsShowAccountForm.siFreqType.value = freqType;
            document.fundsShowAccountForm.siFreqInDays.value = freqInDays;
            document.fundsShowAccountForm.siNoOfPayments.value = noOfPayments;
            document.fundsShowAccountForm.action = "lastfivesitransactions.htm";
        }
        //modified for SI schedule ends
    }
    //Added for CR 5015

    if (txnView == 'showLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'showLastFiveTransactions';
    }
    if (txnView == 'showSILastFiveTxn') {
        document.getElementById('lastFiveSITransactionFlag').value = 'showLastFiveSITransactions';
    }
    if (txnView == 'noShowLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'noShowLastFiveTransactions';
        document.getElementById('lastFiveSITransactionFlag').value = 'noShowLastFiveSITransactions';
    }
    if (creditProductType == "B1") {
        if (txnView == 'showLastFiveTxn')
            alert("If the credit account is a PPF account, the last five transaction details cannot be displayed here.");
    }
    //End of CR 5015

    document.fundsShowAccountForm.creditTxnCount.value = "1";

    document.fundsShowAccountForm.amountTransfer.value = document.fundsTransferForm.TransferAmount.value;

    /*document.fundsShowAccountForm.transactionRemarks.value=document.fundsTransferForm.transactionRemarks.value;*/
    //transactionRemarks();
    if (!transactionRemarks()) {
        return false;
    }
    document.fundsShowAccountForm.debitProductCode.value = debitProductCode;	//	Added for CR 1256

    document.fundsShowAccountForm.creditProductCode.value = creditProductCode;	//	Added for CR 1256	




    document.fundsShowAccountForm.submit();

}



function submitThirdPartyTransferNew(bankCode, txnView) {

    var debitAccountNo = document.fundsShowAccountForm.debitAccountNo.value;

    var creditAccountNo = document.fundsShowAccountForm.creditAccountNo.value;

    var debitBranchCode = document.fundsShowAccountForm.debitBranchCode.value;

    var creditBranchCode = document.fundsShowAccountForm.creditBranchCode.value;

    var amount = document.fundsTransferForm.TransferAmount.value;;

    var thirdPartyLimit = document.fundsShowAccountForm.thirdparty_limit.value;

    var accountLimit = document.fundsShowAccountForm.accountLimit.value;

    var debitProdDesc = "";

    var creditProdDesc = "";

    var debitBankSys = "";

    var creditBankSys = "";

    //	Added for CR 1205

    var debitProductCode = "";

    var creditProductCode = "";

    //	End of CR 1205

    if (document.getElementById("dproductDisc") != null)

        debitProdDesc = document.getElementById("dproductDisc").value;

    if (document.getElementById("cproductDisc") != null)

        creditProdDesc = document.getElementById("cproductDisc").value;

    if (document.getElementById("dbankSystem") != null)

        debitBankSys = document.getElementById("dbankSystem").value;

    if (document.getElementById("cbankSystem") != null)

        creditBankSys = document.getElementById("cbankSystem").value;

    //	Added for CR 1205	

    if (document.getElementById("dproductCode") != null)

        debitProductCode = document.getElementById("dproductCode").value;



    if (document.getElementById("cproductCode") != null)

        creditProductCode = document.getElementById("cproductCode").value;

    //	End of CR 1205

    if (!validateFundsTransfer('credit'))

        return false;



    if (bankCode == 0) {
        if (!isValidTransferForSBI(debitAccountNo, creditAccountNo, 'Pri', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode)) {

            return false;

        }

    }

    else if (!isValidTransfer(debitAccountNo, creditAccountNo, 'Pri', debitProdDesc, creditProdDesc, debitBankSys, creditBankSys, debitProductCode, creditProductCode)) {

        return false;

    }


    if (parseFloat(amount) > parseFloat(accountLimit)) {

        alert("The transfer amount for " + creditAccountNo + " must be less than or equal to Rs." + accountLimit + "/-.");

        document.fundsTransferForm.TransferAmount.focus();

        return false;

    }

    //Commented for CR 5067

    //added for retail CR2633 starts

    var creditProductType = document.fundsShowAccountForm.creditAccountType.value;

    if (creditProductType == "B1") {
        //Added for CR 3081
        //commented by sridhar
        /*var PPFLimit = document.getElementById('PPFMiniLimit').value;	
         var PPFMaxLimit = document.getElementById('PPFMaxLimit').value;
            if(parseFloat(amount)<parseFloat(PPFLimit)){	
            alert("PPF deposit amount should be a minimum of Rs."+PPFLimit+".");
            return false;
            }	*/
        //Added for CR 3081 ends
        //commented by sridhar
        /*if(parseFloat(amount)>parseFloat(PPFMaxLimit))
        {
        alert("The Credit Limit for a PPF Account cannot exceed 777"+PPFMaxLimit+"/- ");
        return false;
        }*/
        if (amount % 5 != 0) {
            alert("Amount should be in multiples of 5");
            return false;
        }

    }

    //modified for SI schedule starts
    //added for PPF cr2633 starts
    if (creditProductType == "B1") {
        document.fundsShowAccountForm.action = "fundstransferdetails.htm";
    } else {
        document.fundsShowAccountForm.action = "lastfivetxndetails.htm";
    }
    // SI schedule ends
    //Added For Retail FT TP Scheduling

    if (document.fundsTransferForm.SchedulePay != null) {
        //modified for SI schedule starts
        if (document.fundsTransferForm.SchedulePay[1].checked || (document.fundsTransferForm.SchedulePay[2] != null && document.fundsTransferForm.SchedulePay[2].checked)) {
            var schDate = document.getElementById("Days").value + "/" + document.getElementById("Months").value + "/" + document.getElementById("Years").value;
            document.getElementById("schPayDate").value = schDate;
        }
        if (document.fundsTransferForm.SchedulePay[2] != null && document.fundsTransferForm.SchedulePay[2].checked) {
            var freqType = document.getElementById("freqType").value;
            var freqInDays = document.getElementById("freqInDays").value;
            var noOfPayments = document.getElementById("noOfPayments").value;
            var accType = eval(document.getElementById("creditAccountType")).value; /* Added for SI PPF account - martin */
            if (!purenumeric('fundsTransferForm', 'noOfPayments', 'Number Of Payments')) {
                return false;
            } else if (eval(noOfPayments) < 2) {
                alert("Number of payments should exceed 1");
                return false;
            }
            /* Added for SI PPF account - martin - Begin */
            if (accType == 'B1' && eval(noOfPayments) > 12) {
                alert("Number of payments should not exceed 12");
                return false;
            }

            if (isERDAccount() && eval(noOfPayments) > 119) {
                alert("Number of payments should not exceed ten years from the e-RD A/c creation date.");
                return false;
            }

            /* Added for SI PPF account - martin - End*/
            var yearInDays = 365 * 24 * 60 * 60 * 1000;
            var datesplit1 = (document.getElementById("schPayDate").value).split("/");
            date = datesplit1[0];
            month = datesplit1[1];
            var monArr = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
            for (var i = 0; i < 12; i++) {
                if (monArr[i] == month) {
                    month = i;
                    break;
                }
            }
            year = datesplit1[2];
            var startDate = new Date(year, month, date);
            var startTime = startDate.getTime();
            var dateoneYearAfter = new Date(startTime + yearInDays);
            var daysAdded = null;
            if (freqType == 'Days') {
                daysAdded = eval(freqInDays) * eval(noOfPayments);
            } else {
                daysAdded = eval(freqInDays) * 30 * eval(noOfPayments);
            }
            var daysAddedInTime = eval(daysAdded) * 24 * 60 * 60 * 1000;
            lastPayDate = new Date(daysAddedInTime + startTime);

            // Modified for CR : 5609 - Parameshwaran - Added conditon
            if (lastPayDate > dateoneYearAfter && !isERDAccount()) {
                alert("Period for standing instruction should not exceed one year");
                return false;
            }

            // Financial Year Calculation
            if (creditProductType == "B1") {
                var schDate = document.getElementById("Days").value + "/" + document.getElementById("Months").value + "/" + document.getElementById("Years").value;
                document.getElementById("schPayDate").value = schDate;

                if (!validatePPFFreq(parseFloat(amount))) {
                    return false;
                }
            }

            // Modified for CR : 5609 - Parameshwaran - Added condition

            document.fundsShowAccountForm.siFreqType.value = freqType;
            document.fundsShowAccountForm.siFreqInDays.value = freqInDays;
            document.fundsShowAccountForm.siNoOfPayments.value = noOfPayments;
            document.fundsShowAccountForm.action = "lastfivesitransactions.htm";
        }
        //modified for SI schedule ends
    }
    //Added for CR 5015		
    if (txnView == 'showLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'showLastFiveTransactions';
    }
    if (txnView == 'showSILastFiveTxn') {
        document.getElementById('lastFiveSITransactionFlag').value = 'showLastFiveSITransactions';
    }
    if (txnView == 'noShowLastFiveTxn') {
        document.getElementById('lastFiveTransactionFlag').value = 'noShowLastFiveTransactions';
        document.getElementById('lastFiveSITransactionFlag').value = 'noShowLastFiveSITransactions';
    }
    if (creditProductType == "B1") {
        if (txnView == 'showLastFiveTxn')
            alert("If the credit account is a PPF account, the last five transaction details cannot be displayed here.");
    }
    //End of CR 5015
    document.fundsShowAccountForm.creditTxnCount.value = "1";

    document.fundsShowAccountForm.amountTransfer.value = document.fundsTransferForm.TransferAmount.value;

    //document.fundsShowAccountForm.transactionRemarks.value=document.fundsTransferForm.transactionRemarks.value;
    //transactionRemarks();
    if (!transactionRemarks()) {
        return false;
    }
    document.fundsShowAccountForm.debitProductCode.value = debitProductCode;	//	Added for CR 1205

    document.fundsShowAccountForm.creditProductCode.value = creditProductCode;	//	Added for CR 1205


    document.fundsShowAccountForm.submit();

}
function transactionRemarks() {
    var selectedItemText = document.fundsTransferForm.transactionRemarks.value;
    if (selectedItemText == "Others") {
        if (document.fundsTransferForm.remarks.value != "") {
            if (!validatemain("fundsTransferForm", "remarks.alphanumerikcheck.Purpose")) {
                document.fundsTransferForm.remarks.focus();
                return false;
            }
            else {
                document.fundsShowAccountForm.transactionRemarks.value = document.fundsTransferForm.remarks.value;
                return true;
            }
        }
        else {
            document.fundsShowAccountForm.transactionRemarks.value = document.fundsTransferForm.remarks.value;
            return true;
        }


    }
    else {
        document.fundsShowAccountForm.transactionRemarks.value = document.fundsTransferForm.transactionRemarks.value;
        return true;
    }

}

function ddRemarks() {
    var selectedItemText = document.demandDraftForm.ddRemarks.value;

    if (selectedItemText == "Others") {
        document.demandDraftValueForm.ddRemarks.value = document.demandDraftForm.remarks.value;
    }
    else {

        document.demandDraftValueForm.ddRemarks.value = document.demandDraftForm.ddRemarks.value;

    }
}

//SINB_8352_To rename a exisitng link View Last Five Transactions and continue
function submitLastFiveTxnDetails() {
    document.getElementById('lastFiveTransactionFlag').value = 'newShowLastFiveTransactions';
    document.fundsShowAccountForm.action = "lastfivetxndetails.htm";
    document.fundsShowAccountForm.submit();
}

function submitFundsTransferingBack(transactionName) {

    if (transactionName == "FT") {
        document.fundsShowAccountForm.action = "fundstransfer.htm";
    }
    else if (transactionName == "TP") {
        document.fundsShowAccountForm.action = "thirdpartytransfer.htm";
    }
    else if (transactionName == "DD") {
        document.fundsShowAccountForm.action = "demanddraft.htm";
    }
    else if (transactionName == "bp") {
        document.fundsShowAccountForm.action = "viewpaywithoutbillshow.htm";
    }
    else if (transactionName == "IDP") {
        document.fundsShowAccountForm.action = "indirect_payment_details.htm";
    }
    else if (transactionName == "VMT") {
        document.fundsShowAccountForm.action = "tempvisamakepayment.htm";
    }
    else if (transactionName == "MD") {       //Added for Make donation Link
        document.fundsShowAccountForm.action = "makedonation.htm";
    }
    else if (transactionName == "EZT") {       //Added for eztrade transfer Link
        document.fundsShowAccountForm.action = "eztradetransferdisplay.htm";
    }
    document.fundsShowAccountForm.submit();
}
function submitFundsTransferCancel() {

    //SINB_8352_To rename a exisitng link View Last Five Transactions and continue
    //document.fundsShowAccountForm.submit();
    document.fundsShowAccountForm.action = "paymenttransferlanding.htm";
    document.fundsShowAccountForm.submit();
    //SINB_8352_To rename a exisitng link View Last Five Transactions and continue
}
//SINB_8352_To rename a exisitng link View Last Five Transactions and continue
