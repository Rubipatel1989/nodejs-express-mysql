const { use } = require("express/lib/application");
const sql = require("./db.js");
// constructor
const User = function (user) {
    this.name = user.name;
    this.age = user.age;
    this.mobile = user.mobile,
        this.email = user.email,
        this.address = user.address,
        this.created = user.created,
        this.createdby = user.createdby
};
User.create = (newUser, result) => {
    sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        console.log("created user: ", { id: res.insertId, ...newUser });
        result(null, { id: res.insertId, ...newUser });
    });
};
User.findById = (id, result) => {
    sql.query(`SELECT * FROM users WHERE id = ${id}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            console.log("found users: ", res[0]);
            result(null, res[0]);
            return;
        }
        // not found Tutorial with the id
        result({ kind: "not_found" }, null);
    });
};
User.getAll = (name, result) => {
    let query = "SELECT * FROM users";
    if (name) {
        query += ` WHERE title LIKE '%${name}%'`;
    }
    sql.query(query, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("user: ", res);
        result(null, res);
    });
};

User.updateById = (id, user, result) => {
    sql.query(
        "update users set name = ?, age = ?, mobile = ?, email = ?, address = ? where id = ?",
        [user.name, user.age, user.mobile, user.email, user.address, id], (err, res) => {
            if (err) {
                console.log("Error ", err);
                result(null, error);
                return;
            }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }
            console.log("Updated User:", { id: id, ...user });
            result(null, { id: id, ...user });
        });
};
User.remove = (id, result) => {
    sql.query("DELETE FROM users where id = ?", id, (err, res) => {
        if (err) {
            console.log("Error:", err);
            result(null, err);
            return;
        }
        if (res.affectedRows == 0) {
            result({ kind: "not_found" }, null);
            return;
        }
        console.log("Deleted User Id", id);
        result(null, res);
    });
};

module.exports = User;