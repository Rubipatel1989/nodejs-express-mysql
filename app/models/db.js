// Start Coding By Pawan For Fetch Connection Details
const mysql = require("mysql");
const dbConfig = require("../config/db.config");
const connection = mysql.createConnection({
    host: dbConfig.HOST,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB
});
// Open mysql connection 
connection.connect(error => {
    if (error) throw error;
    console.log("Successfully connected to the database.");
});
module.exports = connection;


